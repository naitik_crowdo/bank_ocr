import os, re
os.chdir(r"C:\financial_scan\bank_stmt")

import cv2, datetime
from pytesseract import pytesseract as pt
from PIL import Image
from difflib import get_close_matches
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

class initial_read_bank_stmt(object):

	def __init__(self,image_path,debug):
		self.debug = debug
		self.word_buffer = .012
		self.buffer_inp=0.30
		self.accept_words_query = 'conf > 16'
		self.config = ("-l ind --oem 1 --psm 6")
		try:
			self.gray = cv2.imread(image_path) ## Read the image
			self.gray = cv2.cvtColor(self.gray, cv2.COLOR_BGR2GRAY)     ## converting to grayscale
		except:
			raise Exception('Error In Reading Image')
			return -1

	def perform_ocr_on_image(self):
		'''
		__takes__ : image path as input
		__provides__: provides raw ocr text parsed with page_num,block_num,Line_num,word_num and box dimensions with text.

		filter words which have less than 55 percent confidence and sort based on block,line and word result df.
		'''
		##Run tesseract call
		#indo_dat = pt.image_to_data(Image.fromarray(gray),lang='ind',config='--psm 1',output_type=pt.Output.DICT )


		ocr_raw = pt.image_to_data(Image.fromarray(self.gray),config= self.config )
		#ocr_raw = pt.image_to_string(Image.fromarray(gray),config= config )
		#ocr_raw = pt.image_to_data(Image.fromarray(gray),lang='ind',config='--psm 1' )
		img_width = self.gray.shape[1]
		return ocr_raw,img_width


	def create_dataframe_from_ocr_raw(self,ocr_raw):
		'''
		__takes__: takes raw string text which are \n(newline) separated for  rows and \t(tab) for columns.
		__provides__: pandas dataframe created which have aall columns and rows from raw_ocr

		__columns_created__: "level","page_num	","block_num","par_num","line_num	","word_num","left","top","width","height","conf","text"

		'''
		##Create dataframe from extracted text
		indo_split = ocr_raw.split("\n") #split first based on newline character for different rows

		lis = [] #initiate list for body
		for i,text in enumerate(indo_split):
			if i ==0:
				heading = text.split("\t") #column separators are \t(tab delimited)
				if self.debug: print(heading)
			else:
				row = text.split("\t") ##Return is tab separated
				if self.debug: print("Line: {}, Word: {}, Text: {}".format(row[4],row[5],row[11]))
				lis.append(row)

		##Create a dataframe using headings extracted from first row of tesseract call and body as list found
		dataf = pd.DataFrame(lis,columns=heading)

		##Trying to convert all possible column to numeric type if not possible ignore
		dataf = dataf.apply(pd.to_numeric, errors='ignore')


		dataf_filtered = dataf.query(self.accept_words_query)

		## Sort the dataframe based on Blocks then line in blocks then words in line.
		dataf_filtered = dataf_filtered.sort_values(['block_num', 'line_num','word_num'], ascending=[True, True, True])

		return dataf_filtered


	######################### STEP3:: Create Calculated Columns
	def __calc_right(self,row):
		return row['left']+row['width']

	def __calc_center(self,row):
		return row['left']+row['width']/2

	def create_derived_cols(self,ocr_df_in):
		'''
		Create 4 additional derivated columns in df i.e:
			  i)- Right coordinate of word
			 ii)- Center coordinate of word box
			iii)- Text contains numeric content or not(ratio between number and non-number in text shud be >.59 to be True)
			 iv)- Try to correct the numeric identified rows text to Numeric if any other identified characters.
		'''
		ocr_df_in['right'] = ocr_df_in.apply (lambda row: self.__calc_right(row),axis=1)
		ocr_df_in['center'] = ocr_df_in.apply (lambda row: self.__calc_center(row),axis=1)


		#convert text_conv column to uppercase
		ocr_df_in['text'] = ocr_df_in['text'].str.upper()

		ocr_df_in = ocr_df_in[ocr_df_in.left != 0] #drop any record where left value was not captured

		return ocr_df_in

	######################### STEP4:: Find Words near to each other based on image width
	def merge_close_words(self,pd_df_cols,img_width):
		max_word_sep = .012*img_width
		print("Taking {} as word separator".format(max_word_sep))

		all_lines = pd_df_cols.line_num.unique()
		grp = 0
		big_grp = list()
		for line in all_lines:
			ind = list(pd_df_cols[pd_df_cols['line_num'] == line].index)
			left = list(pd_df_cols[pd_df_cols['line_num'] == line].left)
			right = list(pd_df_cols[pd_df_cols['line_num'] == line].right)


			new_grp = list()
			for i,index in enumerate(ind):
				if i == 0: #First entry of every line should start with new group
					grp += 1
					new_grp.append(grp) #so first work is always group
					continue
				if left[i]-right[i-1] < max_word_sep: #check condition if left of current word with right of previous word diff is less than the threshold
					new_grp.append(grp) #it means same group
				else: #otherwise different group
					grp += 1 #so increment to new group
					new_grp.append(grp) #and insert new group value
			big_grp.extend(new_grp) #apend all the groups identified in this line to all big list for groups identifies
		return big_grp


	def create_df_with_combine_words(self,pd_df_cols):
		'''
		Create a list with format :: ["line_num","word_num","left","right",'top',"text"] to be converted into dataframe
		'''
		wrd_grp_unique = pd_df_cols.word_grp.unique()

		big_list = list()
		for grp in wrd_grp_unique:
			left = list(pd_df_cols[pd_df_cols['word_grp'] == grp].left)
			right = list(pd_df_cols[pd_df_cols['word_grp'] == grp].right)
			top = list(pd_df_cols[pd_df_cols['word_grp'] == grp].top)
			text = list(pd_df_cols[pd_df_cols['word_grp'] == grp].text)
			line = list(pd_df_cols[pd_df_cols['word_grp'] == grp].line_num)
			temp_list = (line[0],grp,left[0],right[-1],top[0],' '.join(text))
			big_list.append(temp_list)

		labels = ["line_num","word_num","left","right",'top',"text"]
		return big_list,labels

	def main_till_df(self):
		try:
			raw_df,self.img_width = self.perform_ocr_on_image()

			### STEP2: Create Dataframe
			pd_df = self.create_dataframe_from_ocr_raw(raw_df)

			### STEP3: Find Derived columns
			pd_df_cols = self.create_derived_cols(pd_df)

			### STEP4: Merge near words together
			big_grp = self.merge_close_words(pd_df_cols,self.img_width)	 #return a list of group belongs
			pd_df_cols['word_grp'] = big_grp #add column in df to show group

			### STEP5: Create df with combine words
			big_list,labels = self.create_df_with_combine_words(pd_df_cols)
			df = pd.DataFrame.from_records(big_list, columns=labels)
			return df
		except Exception as e:
			print("Error in Creating Pandas Dataframe")
			print(e)
			raise Exception('Error In OCR/Creating Dataframe')
			return -1


##################################################################################
######################### STEP 6: FIND ROI based on Date(in front) and Number(in last)

class find_ROI(object):

	def __init__(self,debug):
		self.debug = debug
		self.date_clean_re = '[^0-9/]+'
		self.numeric_clean_re = '[^0-9.]+'
		self.numeric_threshold = 0.70

	def __date_type(self,text):
		#step1 - remove any unwanted symbols (only number and slash'/' remains)
		text_clean = re.sub(self.date_clean_re,'', text)
		text_clean_split = text_clean.split('/')
		if len(text_clean_split) in [2,3]: #possible candidate for date type only 01/06 or 01/06/2019 allowed
			split_length_numeric = [len(x) for x in text_clean_split if len(x) >0] #check for condition where only '/' came with no numbers (like 'abc/bcd' or '/')
			if len(split_length_numeric) > 0:
				return True
		return False

	def __numeric_type(self,text):
		#step1 remove any unwanted symbols
		text_clean = re.sub(self.numeric_clean_re,'', text)
		if len(text_clean)/len(text) > self.numeric_threshold and len(text_clean)>2: #if after clean and before clean length ratio is more than 70% then it's type number
			return True
		return False

	def find_date_and_numeric_for_ROI(self,df):
		'''
		This method will try to identify dates in first 2 location and numeric type in last two locations of each line
		and return in deictionary all occurence of found in list of tuple format(instance_position,left,right,top,text)
		'''
		all_lines = df.line_num.unique() #All lines in our df

		# Declare temp dictionary to return
		pos_date = {}
		pos_number = {}

		#iterate for all lines in our df found
		for line in all_lines:
			#create list of content in current line
			line_text = list(df[df['line_num'] == line].text)
			line_left = list(df[df['line_num'] == line].left)
			line_right = list(df[df['line_num'] == line].right)
			line_top = list(df[df['line_num'] == line].top)

			#temp list to hold multiple tuple if:
			#   found two dates in first 2 entry
			#   found two numeric in last two entry
			temp_date = []
			temp_numeric = []
			for i,text in enumerate(line_text):
				if i == 0 or i==1: #check for date in initial part
					#print("Check '{}' for date type".format(text))
					if self.__date_type(text): #check for if this is date type
						if self.debug: print("found '{}' to be date on line: {}".format(text,line))
						tmp_str = (i,line_left[i],line_right[i],line_top[i],line_text[i])
						temp_date.append(tmp_str)

				if i == len(line_text)-1 or i == len(line_text)-2:
					if(self.__numeric_type(text)): #call method to check if it's numeric type
						temp_num = (i,line_left[i],line_right[i],line_top[i],line_text[i]) #create a tuple
						temp_numeric.append(temp_num) #append this into list

			if len(temp_date)>0: #only add into dict if something found
				pos_date[line] = temp_date
			if len(temp_numeric) >0:
				pos_number[line] = temp_numeric
		return pos_date,pos_number

	def find_ROI(self,pos_date,pos_number):
		lines_contain_roi = []
		for line in pos_date:
			if pos_number.get(line,None):
				lines_contain_roi.append(line)

		min_roi = min(lines_contain_roi)
		max_roi = max(lines_contain_roi)
		return (min_roi,max_roi)

	######################### STEP 6B: Finding any stop line based on buffer from heading line
	def find_stop_line_after_heading(self,df,heading_line,buffer_inp=0.30,visualize = False):
		### Find gap betwee all ines and store in dict
		all_lines = list(set(df.line_num))

		'''
		STEP 1 : will find the Gaps between all lines currently present
		'''
		all_diff = {}
		for i in all_lines:
			if i == min(all_lines):
				top_prev = np.mean(list(df[df['line_num'] == i].top))
				all_diff[i] = 0
			else:
				top_cur = np.mean(list(df[df['line_num'] == i].top))
				diff = abs(top_cur- top_prev)
				all_diff[i] = diff
				top_prev = top_cur

		'''
		STEP 2: will try to find after heading line where maximum change in gap present as stop line
		Here buffer used is 0.40% of gap present between lines to check any variations
		'''
		stop_line = 0
		end_found = False
		for k,v in all_diff.items():
			if k == heading_line+2:
				prev = v
				buffer = buffer_inp*v
				start_val= v
			elif k > heading_line+2 and not end_found:
				cur  = v
				diff = abs(cur-prev)
				if diff > buffer:
					end_found = True
					stop_line = k

		'''
		Step 3: Visualize the gap between lines for understanding purpose
		'''
		if visualize:
			##Visualize the Gap between lines
			lists = sorted(all_diff.items()) # sorted by key, return a list of tuples
			x, y = zip(*lists) # unpack a list of pairs into two tuples

			plt.figure(figsize=(15,8))
			plt.plot(x, y)
			plt.axvline(x=heading_line,color='r')
			plt.text(heading_line-0.5,max(y)/2,"HEADING Line", rotation = 90, verticalalignment = 'center',fontsize=14)

			if stop_line!=0:
				plt.axvline(x=stop_line,color='g')
				plt.text(stop_line-0.5,max(y)/2,"STOP Line", rotation = 90, verticalalignment = 'center',fontsize=14)
				plt.text((max(x)+heading_line)/2,start_val+buffer,str(buffer_inp)+"% BUFFER", horizontalalignment = 'center',fontsize=14)
			else:
				plt.axvline(x=max(x),color='g')
				plt.text(max(x)-0.5,max(y)/2,"DEFAULT STOP Line", rotation = 90, verticalalignment = 'center',fontsize=14)
				plt.text((max(x)+heading_line)/2,start_val+buffer,str(buffer_inp)+"% BUFFER", horizontalalignment = 'center',fontsize=14)
				stop_line = max(x)
			#plt.axhspan(start_val-buffer, start_val+buffer, facecolor='r', alpha=0.5)

			plt.fill_between((heading_line+2,stop_line),start_val-buffer, start_val+buffer,color='c', alpha=0.5)
			plt.xticks(np.arange(min(x), max(x)+1, 1))
			plt.xlabel('Line number',fontsize = 14)
			plt.show()

		return max(all_lines) if stop_line ==0 else stop_line

	######################### STEP6C: Find the extra line on top(previous balance) or last(Continue Description)- extend ROI

	def check_min_extended_line(self,df,min_line,i):
		'''
		Just above the minimum area line check for if any previous balance term mention so as to include that line in ROI
		Check Include
		 * presence of word SALDO
		 * Presence of some numeric type in same line
		 '''
		pot_head_line = list(df[df['line_num'] == min_line-i].text)
		saldo_cond = False
		numeric_cond = False
		for text in pot_head_line:
			if 'SALDO' in text.split(' '):
				saldo_cond = True
			elif self.__numeric_type(text):
				 numeric_cond = True
		return 	saldo_cond,numeric_cond


	######################### STEP7: Find Heading line and attributes if any from 2 lines above ROI
	def find_possible_heading_tags_in_2_line_above_min(self,df,poss_line):
		words_to_check = ['TRANS','DATE','TANGGAL','VALUTA','VALUE',"KETERANGAN","RINCIAN","TRANSAKSI","DESCRIPTION","REFF","REFERENCE",'REFERENSI',"DEBIT","KREDIT","CBG","MUTASI","SALDO","BALANCE"]
		corresponding_heading = {
				'TRANS':'DATE',
				'DATE':'DATE',
				'VALUTA':'DATE',
				'VALUE':'DATE',
				'KETERANGAN':'DESCRIPTION',
				"DESCRIPTION":"DESCRIPTION",
				"RINCIAN":'DESCRIPTION',
				"TRANSAKSI":"DESCRIPTION",
				"REFF":'REFERENCE',
				"REFERENCE":'REFERENCE',
				'REFERENSI':'REFERENCE',
				"DEBIT":"DEBIT",
				"KREDIT":'CREDIT',
				"SALDO":'BALANCE',
				"BALANCE":'BALANCE',
				'MUTASI':'DEBIT_CREDIT'
				}

		line_match_value = {}
		for line in poss_line:

			line_text = list(df[df['line_num'] == line].text)
			line_left = list(df[df['line_num'] == line].left)
			line_right = list(df[df['line_num'] == line].right)
			line_top = list(df[df['line_num'] == line].top)
			for i,wrd_line_text_all in enumerate(line_text): ## For all text column in that line
				for wrd_line_text in wrd_line_text_all.split(): ## in some cases there might be multiple words combined earlier due to proximity
					closest_word_frm_heading_lists = get_close_matches(wrd_line_text,words_to_check, 1, 0.65) #find closest matching word if any from our list of words
					if len(closest_word_frm_heading_lists)>0: #if found something
						known_heading = corresponding_heading.get(closest_word_frm_heading_lists[0],None) #Get proper english name for that identified word
						if len(known_heading)>0: #if proper english name returned(should do always)
							if line_match_value.get(known_heading,'None') == 'None': #means fresh value for current heading(first entry for heading)
								line_match_value[known_heading] = (line_left[i],line_right[i],line_top[i],wrd_line_text) #Create heading Entry
								if self.debug: print("\nFound Matching heading for '{}' is '{}'".format(wrd_line_text,known_heading))

							else: #It means current heading already stored but we will see the left and right of current value to maximize
								if self.debug: print("\nAlready Heading existed for '{}' is '{}'".format(wrd_line_text,known_heading))

								left_stor,right_stor,top_stor,text_stor = line_match_value.get(known_heading) #values for existing heading
								left_now, right_now, top_now = line_left[i], line_right[i], line_top[i] #value for current entry

								if known_heading == 'DATE': #special case for Date
									#check if some pixels overlapping then do normal otherwise make new entry
									if left_stor <= left_now <= 	right_stor or 	left_stor <= right_now <= 	right_stor or left_now <= left_stor <= right_now:
										if left_now<left_stor:
											if self.debug: print("\t\tUpdating Left value from {} to {}".format(left_stor,left_now))
											left_stor=left_now
										if right_now>right_stor:
											if self.debug: print("\t\tUpdating Right value from {} to {}".format(right_stor,right_now))
											right_stor = right_now
										#finally update the changed value if any changes happened new value will be used otherwise old update
										line_match_value[known_heading] = (left_stor,right_stor,top_stor,text_stor)
									else:
										if self.debug: print("\tCreating New entry for DATE1")
										line_match_value['DATE1'] = (left_now,right_now,top_now,wrd_line_text)

								else:
									print("\tThis heading:{} should not repeat, avoiding this".format(wrd_line_text))
									print("\tExisting group details: left: {}, right: {}".format(left_stor,right_stor))
									print("\tNew group details: left: {}, right: {}".format(left_now,right_now))
									'''
									if self.debug: print('\tbut trying to see boundaries of both entries set to max')

									if left_now<left_stor:
										if self.debug: print("\t\tUpdating Left value from {} to {}".format(left_stor,left_now))
										left_stor=left_now
									if right_now>right_stor:
										if self.debug: print("\t\tUpdating Right value from {} to {}".format(right_stor,right_now))
										right_stor = right_now
									#finally update the changed value if any changes happened new value will be used otherwise old update
									line_match_value[known_heading] = (left_stor,right_stor,top_stor,text_stor)
									'''
						else:
							print("Not equivalent english name found in 'corresponding_heading' for ",closest_word_frm_heading_lists[0])
		return line_match_value

	################################################################################
	###################### STEP 7B: Merge Heading attributes if found overlapping'

	## Need to merge the headings came if in same pixel range but different names:
	## like DEBIT and CREDIT can be 2 keys but if pixel range overlaps then supposedly DEBIT_CREDIT category

	def __get_heading_dict_entry(self,heading_grp_extracted,to_rep,for_rep):
		'''
		Sub-function used to extract information for group custom list of set made earlier(heading_grp_extracted)
		'''
		(left_1,right_1,top_1,text_1) = heading_grp_extracted.get(to_rep)
		(left_2,right_2,top_2,text_2) = heading_grp_extracted.get(for_rep)

		left_new = min(left_1,left_2)
		right_new = max(right_1,right_2)
		top_new = max(top_1,top_2)
		text_new = text_1+' '+text_2

		return (left_new,right_new,top_new,text_new)

	def check_any_overlapping_heading_exist_then_replace(self,heading_grp_extracted):
		'''
		First part will scan all combination to find any overlapping heading
		'''
		if self.debug: print("Checking in heading if any other heading pixels consides\n-------------------------------------------------")
		head = [k for k in heading_grp_extracted]
		to_merge = []
		for i,heading in enumerate(head):
			if i < len(head)-1:
				left_outer = heading_grp_extracted[heading][0]
				right_outer = heading_grp_extracted[heading][1]
				if self.debug: print("\nfor head: {:10s} \t left:{} \t right:{}".format(heading,left_outer,right_outer))
				for j in range(i+1,len(head)):
					left_inner = heading_grp_extracted[head[j]][0]
					right_inner = heading_grp_extracted[head[j]][1]
					if self.debug: print("\tcheck: {:15s} \t left:{} \t right:{}".format(head[j],left_inner,right_inner))

					if left_outer<=left_inner<=right_outer or left_outer<=right_inner<=right_outer or left_inner<=left_outer<=right_inner:
						to_merge.append((head[i],head[j]))
						if self.debug: print("\t\tHappen Inside, need to replace")
		'''
		Second part will take all overlapping and make changes in dictionary if present
		Currently only check for combinations of:
			* CREDIT and DEBIT
			* DESCRIPTION and REFERENCE
			* Any other combination needs to further analyze(but to make it run now will delete duplicate entry)
		'''
		if len(to_merge)>0:
			for (for_rep,to_rep) in to_merge:
				if self.debug: print("\nReplacing {:10s} with {:10s}".format(to_rep,for_rep))
				if (to_rep=='CREDIT' or for_rep=='CREDIT') and (to_rep=='DEBIT' or for_rep=='DEBIT'):
					# Case of CREDIT_DEBIT
					heading_grp_extracted['DEBIT_CREDIT'] = self.__get_heading_dict_entry(heading_grp_extracted,to_rep,for_rep)
					del heading_grp_extracted[to_rep]
					del heading_grp_extracted[for_rep]
				elif (to_rep=='DESCRIPTION' or for_rep=='DESCRIPTION') and (to_rep=='REFERENCE' or for_rep=='REFERENCE'):
					# Case of DESCRIPTION/REFERENCE
					heading_grp_extracted['DESCRIPTION'] = self.__get_heading_dict_entry(heading_grp_extracted,to_rep,for_rep)
					del heading_grp_extracted['REFERENCE']
				else: #Any other case needs to be analyzed further for coding, but now just removing one heading and keeping other
					if self.debug: print("\n\t------------\tThis combination need further analysis.. PLEASE CHECK ..\n\t---------------")
					heading_grp_extracted[for_rep] = self.__get_heading_dict_entry(heading_grp_extracted,to_rep,for_rep)
					del heading_grp_extracted[to_rep]
		else:
			if self.debug: print("\n*** No group found Overlapping ***")

		return heading_grp_extracted


	## Main block
	def main_ROI_region(self,df):
		pos_date,pos_number =self. find_date_and_numeric_for_ROI(df)
		min_line,max_line = self.find_ROI(pos_date,pos_number)

		max_line_using_buffer = self.find_stop_line_after_heading(df,heading_line=min_line-1,buffer_inp=0.30,visualize = True)
		#if max_line_using_buffer == max_line+2 or max_line_using_buffer == max_line+1: #if there exist 1 more line in between 2 ending
		#	max_line=max_line_using_buffer #accept the buffer end line

		############ Believing in Buffer logic more than any other thing
		print("Using date max line of ROI is {}, but using buffer: {}".format(max_line,max_line_using_buffer))
		max_line = max_line_using_buffer

		stop_line = max_line ##assign to variable for previous code consistency

		# now got first and last line,, try to attempt get heading line also
		min_line_ext = min_line
		for i in range(1,4):
			## check for previous balance
			flag1,flag2 = self.check_min_extended_line(df,min_line,i)
			if self.debug: print(flag1,flag2)
			if flag1 and flag2: #if both flag returned true for saldo and numeric then it's previous balance
				 min_line_ext =  min_line-i

		## Try to find some heading values
		poss_line = [min_line_ext-1,min_line_ext-2] #taking 2 lines above ROI min line found
		heading_line = min_line_ext-1 #assuming 1 line above will be heading line for further calculations

		#Create Dictionary of headings identified
		heading_grp_extracted = self.find_possible_heading_tags_in_2_line_above_min(df,poss_line)
		print(heading_grp_extracted)

		#Now to check if any headings identified have overlapping pixels so to merge those
		## Possible is CREDIT/DEBIT: will be having CREDIT and DEBIT as 2 groups but with same pixels values
		if len(heading_grp_extracted) >0:
			heading_grp_extracted = self.check_any_overlapping_heading_exist_then_replace(heading_grp_extracted)
		else:
			print("Not able to find any Suitable headings from 2 line above ROI: ",min_line_ext)

		return heading_line,min_line_ext,stop_line,heading_grp_extracted

##################################################################################
################################################################################
######################### STEP8: Find the Vertical Group and assign values of group under
class closeness_new(object):
	'''
	This class will try to assign groups to the text using coordinates of box with x-axis(vertical check)
	Primarily check for 3 conditions;
		i)  - Left Pixels are aligned
		ii) - Right pixels are aligned
		iii)- if not both then center of text box aligned

	'''
	def __init__(self,debug):
		self.debug = debug
		self.blk_n = 0
		self.grp ={}
		self.grp_num =1
		#self.found_starting_word = False
		#self.starting_wrds = ["REFERENCE","DESCRIPTION","DEBIT","CREDIT","DEBET"]
		self.buffer = 34
		self.heading_grp = list()
		self.glb_grp_chng = {}
		self.reverse_heading_map = {}


	def check_under_other_grp(self,point_check,grp_exclude):
		for k,v in self.grp.items():
			if k != grp_exclude:
				right_grp = v['right']
				left_grp = v['left']
				#left_in <= left_grp <= right_in and left_in <= right_grp <= right_in
				if left_grp <= point_check <= right_grp:
				#if point_check >= left_grp and point_check <= right_grp:
					return True
		return False

	def under_other_grp_name(self,point_check,grp_exclude):
		for k,v in self.grp.items():
			if k != grp_exclude:
				right_grp = v['right']
				left_grp = v['left']
				if left_grp <= point_check <= right_grp:
					return k
		return False

	def action_grp_extend(self,cur_check_grp,overlap_grp,cur_grp_bound,f,left=False,right=False):
		grp_to_merge_list = []
		grp_to_merge_dict= {}
		group_expansion = {}
		if right == True:
			if overlap_grp not in self.heading_grp: #the other group doesn't belong to heading group means added later and should be merged with heading group
				f.write("\t\t\tOverlap on Right side\n")
				grp_to_merge = overlap_grp #identified group which need to be merge with heading group and remove entry from self.grp_items
				grp_to_merge_list.append(grp_to_merge) #add this grp to not parse again
				grp_to_merge_dict[grp_to_merge] = cur_check_grp
				self.glb_grp_chng[grp_to_merge] = cur_check_grp
				group_expansion[cur_check_grp] = {'right':max(self.grp[grp_to_merge]['right'],cur_grp_bound)} #need to change the dimensions of group
				f.write("\t\texpansion also needed:{}\n".format(str(group_expansion)))
			else:
				print("Overlapping group: {} is part of Heading groups formed: {}\n".format(overlap_grp,self.heading_grp))
				f.write("\t\tOverlapping group: {} is part of Heading groups formed: {}\n".format(overlap_grp,self.heading_grp))
				## If this message comes then we need to think about merging those heading also or not

		if left == True:
			if overlap_grp not in self.heading_grp: #the other group doesn't belong to heading group means added later and shoould be merged with heading group
				f.write("\t\t\tOverlap on left side\n")
				grp_to_merge = overlap_grp #identified group which need to be merge with heading group and remove entry from self.grp_items
				grp_to_merge_list.append(grp_to_merge) #add this grp to not parse again
				grp_to_merge_dict[grp_to_merge] = cur_check_grp
				self.glb_grp_chng[grp_to_merge] = cur_check_grp
				group_expansion[cur_check_grp] = {'left':min(self.grp[grp_to_merge]['left'],cur_grp_bound)} #need to change the dimensions of group
				f.write("\t\texpansion also needed:{}\n".format(str(group_expansion)))
			else:
				print("Overlapping group: {} is part of Heading  groups formed: {}\n".format(overlap_grp,self.heading_grp))
				f.write("\t\tOverlapping group: {} is part of Heading groups formed: {}\n".format(overlap_grp,self.heading_grp))
				## If this message comes then we need to think about merging those heading also or not
		return grp_to_merge_list,grp_to_merge_dict,group_expansion

	def create_possible_grp_dict_wd_wdout_expansion(self,k,left_grp,right_grp,left_in,right_in,group_expansion=False):
		'''
		This method will return psossible_group dict value for 'k' group by checking closest left or right for value
		If with expansion it will check value after expansion otherwise without expansion also normal check.
		'''
		fin_dict = {}
		if group_expansion:
			if group_expansion.get(k,None):
				#it means expansion is required for this group
				if group_expansion[k].get('left',None):
					#it means left expansion is there
					left_grp = group_expansion[k]['left']
				if group_expansion[k].get('right',None):
					#it means right expansion is there
					right_grp = group_expansion[k]['right']

		#This will happen everytime this method called
		#Means no expansion is there just find min vlaue and return the dict
		dist_frm_left = abs(left_grp-left_in)
		dist_frm_right = abs(right_grp-right_in)

		if dist_frm_left < dist_frm_right:
			fin_dict['distance'] = abs(left_grp-left_in)
			fin_dict['method'] = 'left'
		else:
			fin_dict['distance'] = abs(right_grp-right_in)
			fin_dict['method'] = 'right'

		return fin_dict

	def check_closeness(self,left_in,right_in,f,debug=False):
		'''use external dictionary "grp", which will be in format
			grp[grp_num] = {'left' : left, 'right' : right}

			return possible_grp = {'distance':22, 'outlier':0}
						 expansion = {'left':10,'right':0}
		'''
		possible_grp ={}
		group_expansion = {}
		grp_to_merge_list = list()
		grp_to_merge_dict = {}
		#expansion_left = {}
		#expansion_right = {}
		#expansion = {0:{'left':0,'right':0}}
		#centre_in = (left_in+right_in)/2
		f.write("================================================\n")
		f.write("Checking for points: left-{}, right-{}\n".format(left_in,right_in))
		#print("Input left right from df : left:{}, right:{}".format(left_in,right_in))
		for k,v in self.grp.items():
			if k not in grp_to_merge_list:

				right_grp = v['right']
				left_grp = v['left']
				f.write("\tmatching with group:{}, left:{},right:{}\n".format(k,left_grp,right_grp))
				#width_grp = right_grp-left_grp
				#centre_grp = (right_grp + left_grp)/2

				############ CASE DEFAULT - Both Points inside ###########################
				if left_grp <= left_in <= right_grp and left_grp <= right_in <= right_grp:
					f.write("\t\tLeft_in and right_in inside group\n")

					possible_grp[k] = self.create_possible_grp_dict_wd_wdout_expansion(k,left_grp,right_grp,left_in,right_in,group_expansion=False)

					############ CASE 1 - more on right ###########################
					'''This is triggered when Left Point of word(left_in) , falls under the range of current group limits(left_grp , right_grp)

				Under this 2 conditions can arrive:
					1. Right_in (right extreme of word) falls within group limit
					2. Right_in falls outside the group limit:
						2.1: It falls outside group limit but not overlapping with other group:
									-Just extend group boundaries also
						2.2: It falls outside and also overlapping with some other group boundary condition
									-
						'''
				#if left_in>left_grp and left_in < right_grp: #it means left is inside of group length
				elif left_grp <= left_in <= right_grp: #it means left in between group ranges    " |  *-------|------* "  #here '|' <- represent group_boundary and '*' <- in_point
					f.write("\t\tLeft_in inside group\n")
					if not self.check_under_other_grp(right_in,k): #it means not exploiting any other group boundary so can be increased size

						if right_in > right_grp: #input right is not touching any other boundary as well as right is longer than current group size
							group_expansion[k] = {'right':right_in} #longer word boundary taken as group boundary now
							f.write("\t\texpansion also needed:{}\n".format(str(group_expansion)))
							### Below function will find the possible group now with doing expansion now
							possible_grp[k] = self.create_possible_grp_dict_wd_wdout_expansion(k,left_grp,right_grp,left_in,right_in,group_expansion=group_expansion)
							f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))

						else: #it means right expansion is not required and normal possible group extracted
							possible_grp[k] = self.create_possible_grp_dict_wd_wdout_expansion(k,left_grp,right_grp,left_in,right_in,group_expansion=False)
							f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))
						#possible_grp[k] = {'distance':abs(left_grp-left_in),'method':'left'}
						#f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))
					else: #it means it's into some other group boundary also on right, since left inside

						####### STEP1 :: Find the Overlapping group Names on Right here
						overlap_grp_right = self.under_other_grp_name(right_in,k) # find overlapping group on right if any
						f.write("\t\tOther group Overlap: {}, left:{}, right:{}\n".format(overlap_grp_right,self.grp[overlap_grp_right]['left'],self.grp[overlap_grp_right]['right']))

						####### STEP2 :: Find the group_expansion dictionary(to expand the overlapping heading group) and which group to merge dict/list
						grp_to_merge_list,grp_to_merge_dict,group_expansion = self.action_grp_extend(cur_check_grp = k,overlap_grp = overlap_grp_right,cur_grp_bound=right_grp,f=f,left=False,right=True) #give dictionary for expansion and which group to merge

						####### STEP3 :: Once we have expansion dict we want to find the possible group for this row values
						possible_grp[k] = self.create_possible_grp_dict_wd_wdout_expansion(k,left_grp,right_grp,left_in,right_in,group_expansion=group_expansion)
						f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))

				############ CASE 2 - more on left ###########################
				#elif right_in < right_grp and right_in > left_grp:
				elif left_grp <= right_in <= right_grp : #it means right end lie in between group ranges  " *------|-----*      | "
					f.write("\t\tright_in inside group\n")
					if not self.check_under_other_grp(left_in,k): #it means not exploiting any other group boundary so can be increased size
						possible_grp[k] = {'distance':abs(right_grp-right_in),'method':'right'}
						f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))
						if left_in < left_grp: #input left is not touching any other boundary as well as left is shorter than current group size
							group_expansion[k] = {'left':left_in}
							f.write("\t\texpansion also needed:{}\n".format(str(group_expansion)))

					else: #it means it's into some other group boundary also on left, since right inside

						overlap_grp_left = self.under_other_grp_name(left_in,k) # find overlapping group on right if any
						f.write("\t\tOther group Overlap: {}, left:{}, right:{}\n".format(overlap_grp_left,self.grp[overlap_grp_left]['left'], self.grp[overlap_grp_left]['right']))

						grp_to_merge_list_1,grp_to_merge_dict_1,group_expansion_1 = self.action_grp_extend(cur_check_grp = k,overlap_grp = overlap_grp_left,cur_grp_bound=left_grp,f=f,left=True,right=False) #give dictionary for expansion and which group to merge overlapping

						possible_grp[k] = self.create_possible_grp_dict_wd_wdout_expansion(k,left_grp,right_grp,left_in,right_in,group_expansion=group_expansion)
						f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))

				############ CASE 3 - extended on both sides###########################
				elif left_in <= left_grp <= right_in and left_in <= right_grp <= right_in: #   " *------|---------|------*   " #now text left and right_in fall under region but not inside of group limits just cover the area of group.
					f.write("\t\tgroup inside left_in and right_in\n")
					if not self.check_under_other_grp(left_in,k) and not self.check_under_other_grp(right_in,k): #it means not exploiting any other group boundary so can be increased size, simple here as no other group boundary claims this new entry.
						f.write("\t\tNo other group boundary violated\n")
						if abs(right_grp-right_in) < abs(left_grp-left_in):
							possible_grp[k] = {'distance':abs(right_grp-right_in),'method':'right'}
							f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))

						else:
							possible_grp[k] = {'distance':abs(left_grp-left_in),'method':'left'}
							f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))

						# Since already known that left_in and right_in are out so add to expansion part,
						group_expansion[k] = {'left':left_in,'right':right_in}
						f.write("\t\texpansion also needed:{}\n".format(str(group_expansion)))

					else: #tricky now as some other group boundary also claim this text region.
						f.write("\t\tSome other group boundary violated\n")

						if k in self.heading_grp: #this means that this k is from heading groups
							f.write("\t\t\t{}: group is from heading group\n".format(k))
							overlap_grp_left = self.under_other_grp_name(left_in,k) #overlapping group on left if any
							overlap_grp_right = self.under_other_grp_name(right_in,k) #overlapping group on right if any
							f.write("\t\t\tOverlapping group on Left:{} or Right:{}\n".format(overlap_grp_left,overlap_grp_right))

							######### CASE 1- BOTH SIDES #############   " |    GP1_left  *---|---------|---GP2---|------|---* GP3_right    | "
							if overlap_grp_left != False and overlap_grp_right != False: #it means both side of group has overlapping groups
								#First check for overlap on right
								grp_to_merge_list,grp_to_merge_dict,group_expansion = self.action_grp_extend(cur_check_grp = k,overlap_grp = overlap_grp_right,cur_grp_bound=right_grp,f=f,left=False,right=True)

								#Then check for overlap on left
								grp_to_merge_list_1,grp_to_merge_dict_1,group_expansion_1 = self.action_grp_extend(cur_check_grp = k,overlap_grp = overlap_grp_left,cur_grp_bound=left_grp,f=f,left=True,right=False)

								#Merge both results into 1 for final list and dict
								grp_to_merge_list.append(grp_to_merge_list_1)
								grp_to_merge_dict.update(grp_to_merge_dict_1)
								group_expansion.update(group_expansion_1)
								#add this group to possible group dict
								possible_grp[k] = self.create_possible_grp_dict_wd_wdout_expansion(k,left_grp,right_grp,left_in,right_in,group_expansion=group_expansion)
								f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))

							######### CASE 2- RIGHT SIDE #############    " |    GP1_left     |        |    GP2 *---|------|---* GP3_right    | "
							elif overlap_grp_left == False and overlap_grp_right != False: #it means only right side value overlapping
								grp_to_merge_list,grp_to_merge_dict,group_expansion = self.action_grp_extend(cur_check_grp = k,overlap_grp = overlap_grp_right,cur_grp_bound=right_grp,f=f,left=False,right=True)
								#possible_grp[k] = {'distance':abs(left_grp-left_in),'method':'left'}
								#add this group to possible group dict
								possible_grp[k] = self.create_possible_grp_dict_wd_wdout_expansion(k,left_grp,right_grp,left_in,right_in,group_expansion=group_expansion)
								f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))

							######### CASE 3- LEFT SIDE #############   " |    GP1_left  *---|---------|---* GP2     |       |      GP3_right    | "
							elif overlap_grp_left != False and overlap_grp_right == False: #it means only left side value overlapping
								grp_to_merge_list,grp_to_merge_dict,group_expansion = self.action_grp_extend(cur_check_grp = k,overlap_grp = overlap_grp_left,cur_grp_bound=left_grp,f=f,left=True,right=False)
								possible_grp[k] = self.create_possible_grp_dict_wd_wdout_expansion(k,left_grp,right_grp,left_in,right_in,group_expansion=group_expansion)
								f.write("\t\tAdding to possible group: {}\n".format(str(possible_grp)))

		## Expand the current group value
		if len(group_expansion) != 0: #check if any value present
			f.write("\tDoing Expansion\n")
			for k,v in group_expansion.items(): #extract group in k
				for side,value in v.items(): #extract side='left' or 'right' from v and their value to update in self.grp
					self.grp[k][side] = value

		## Merge the groups
		if len(grp_to_merge_dict) != 0:
			for k,v in grp_to_merge_dict.items():
				self.grp.pop(k, None)
				f.write("\tMerged group: {} with: {}\n".format(k,v))
				f.write("\t\t"+str(possible_grp)+'\n')
				#Also check if going in possible group so replace
				if len(possible_grp) == 0:
					if self.debug: print("Possible_grp not exist")
					f.write("Possible_grp not exist")
				try:
					if possible_grp.get(k) and not possible_grp.get(v):
						possible_grp[v] = possible_grp.pop(k)
					elif possible_grp.get(k):
						possible_grp.pop(k)
				except:
					pass
		return possible_grp


	def find_closest_among_possible_groups(self,poss_grp):
		'''
		sorted(poss_grp.items(), key=lambda kv: kv[1]['distance']) will sort based on key 'distance' inside dictionary
		----> [(0, {'distance': 11, 'method': 'right'}),
				   1, {'distance': 9, 'method': 'left'})]
		[0] is added to get top element among sorted list  ---> (1, {'distance': 9, 'method': 'left'})
		[0] is added to get the first element(i.e group number) among the top element tuple ---> 1
		'''
		return sorted(poss_grp.items(), key=lambda kv: kv[1]['distance'])[0][0]

	def calc_vertical_group(self,row,heading_line,stop_line,heading_calc=False):
		'''
		Give grouping number to each numeric entry in dataframe.

		First numeric will be started with 1 and if subsequent any numeric value falls within range of this group
		, this group number  will be assigned to that entry
		else, New group is identified and continue for all
		'''
		left = row['left']
		right = row['right']

		#For top heading line initiate each entry with groups
		if row['line_num'] == heading_line and heading_calc == False: #if line is from headings line identified earlier
			if self.blk_n == 0: #means first entry
				self.grp[self.grp_num] = {'left' : left, 'right' : right, 'count':1} #initiate the dict with left,right and count in this group
				self.blk_n =1
				self.heading_grp.append(self.grp_num)
				return self.grp_num
			else:
				self.grp_num += 1 #increment the group to be assigned
				self.grp[self.grp_num] = {'left':left,'right':right,'count':1} #initiate the new group identified
				self.heading_grp.append(self.grp_num)
				return self.grp_num


		elif row['line_num'] > heading_line and row['line_num'] < stop_line: #if line is from headings line identified earlier
			#blok = row['block_num']
			#lin = row['line_num']
			#wor = row['word_num']
			logname = "log_temp.txt"
			with open(logname,'a+') as f:
				f.write("\nChecking for word: {}\n".format(row['text']))
				poss_grp = self.check_closeness(left,right,f)

				if len(poss_grp)>1:
					f.write("\t>>>group Identified:: {}\n".format(self.find_closest_among_possible_groups(poss_grp)))
					return self.find_closest_among_possible_groups(poss_grp) #return the group identified by smallest distance if more than 1 identified

				elif len(poss_grp)==1:
					#only 1 group identified and can be used directly
					grp_found = int(list(poss_grp.keys())[0])
					grp_found_cnt_before = self.grp[grp_found]['count']
					self.grp[grp_found]['count'] = grp_found_cnt_before+1
					f.write("\t>>>>group identified:: {}\n".format(grp_found))
					return grp_found #return single group identified
				else:
					#nothing returned from current group so can be contender for new group
					self.grp_num += 1 #increment the group to be assigned
					self.grp[self.grp_num] = {'left':left,'right':right,'count':1} #initiate the new group identified
					f.write("\t>>>>NEW<<<< group identified:: {}\n".format(self.grp_num))
					return self.grp_num

		else:
			return 999

	def set_heading_grp_found_earlier_in_class(self,heading_grp_extracted):

		if len(heading_grp_extracted) >0:
			#change some variables value and initiate new for class as now Heading we are giving manually instead of using class to find
			self.blk_n =1 #because first value we will be creating
			self.grp_num = 0 #initiate from 0 now
			for heading,head_det in heading_grp_extracted.items():
				(left,right,top,orig_text) = head_det
				self.grp_num += 1 #because now initiated with 0 so increment by 1
				self.grp[self.grp_num] = {'left' : left, 'right' : right, 'count':1} #inititate the group entry
				self.heading_grp.append(self.grp_num) #append other dict to know all heading group only
				self.reverse_heading_map[self.grp_num] = heading #for reverse find later which group belong to which group
		return True

	'''
	def find_limit_of_grp_identified_in_df(self,df,heading_grp_extracted,extra_groups_identified, heading_line,min_line_ext):
	#		''
	#		This method will update the bottom(for box) value for Heading groups if found already before(using the first line of body)
	#
	#		Also it will add to the heading group dict if any other group also identified while running the closeness call.
	#
	#		For eg.
	#		heading_grp_extracted = {'BALANCE': (1879, 1994, 1051, 'BALANCE'),
	#											  DATE': (26, 89, 1054, 'DATE'),
	#											  DATE1': (132, 260, 997, 'VAL'),
	#											  DEBIT_CREDIT': (1371, 1610, 1053, 'DEBET CREDIT'),
	#											  DESCRIPTION': (516, 1082, 1052, 'REFERENCE DESCRIPTION')}
	#		So total 5 groups identified here(which can be seen in reverse_heading_map =
	#									    {'DATE', 2: 'DESCRIPTION', 3: 'BALANCE', 4: 'DATE1', 5: 'DEBIT_CREDIT'}
	#
	#	  Now we have uniques group identified in dataframe are [1,2,3,4,5,6,7]
	#
	#		So group 6,7 are additional group found in system call and need to have entry in 	heading_grp_extracted for this as well as in
	#		reverse_heading_map to map the value.
	#		)
	#		''

		################## PART1 :: for existing found heading
		#This will be top coordinate where body area starts
		body_top_min = min(list(df[df['line_num'] == min_line_ext].top)) -2 #also bottom for heading

		##Below conditions will try to find the top for heading section
		if len(heading_grp_extracted) > 0: #it means we found some heading from 2 lines less than min
			min_heading_top = min([v[2] for k,v in heading_grp_extracted.items()])
			##Also use current system to incorporate the bottom of group from top of body line
			for heading in heading_grp_extracted:
				(left,right,top,text) = heading_grp_extracted[heading]
				heading_grp_extracted[heading] = (left,right,min_heading_top,body_top_min,text) #additional body_top_min added here and heading_top same for every heading
		elif heading_line>1:
			min_heading_top = min(list(df[df['line_num'] == heading_line].top))
		else:
			min_heading_top = 3 #something to not fail

		################## PART2: For New groups Identified
		if len(extra_groups_identified) > 0: #if exist some extra groups we found other than identified
			for extra_grp in extra_groups_identified:
				grp_left_ll = list(df[df['grp_match_new'] == extra_grp].left)
				grp_right_ll = list(df[df['grp_match_new'] == extra_grp].right)

				grp_left,grp_right = min(grp_left_ll), max(grp_right_ll) #if headings not identified then the minimum top will become bottom of heading set(first point in body)
				text = 'unknown_'+str(extra_grp) #unknown heading name
				heading_grp_extracted[text] = (grp_left,grp_right,min_heading_top,body_top_min,text)

				self.reverse_heading_map[extra_grp] = text

		return heading_grp_extracted
'''
	def find_nd_upd_limits_of_grp_identified_in_df(self,df,heading_grp_extracted,min_line_ext):
		'''
		This method will find all the group headings extracted(known) or group identified(in closeness call) and update the
		heading_grp_extracted dictionary with corresponding left,right,top,bottom limits(can be extended for known headings also during closeness call or just new entry).
		'''
		head_bottom_max = min(list(df[df['line_num'] == min_line_ext].top)) -2 #also bottom for heading is min top of body
		head_top_min = min([v[2] for k,v in heading_grp_extracted.items()])-2 #also bottom for heading

		for grp,val in self.grp.items():
			if heading_grp_extracted.get(self.reverse_heading_map.get(grp,None),None):
				if self.debug: print("Updating Limits for group: {:^6}, named: {:>15}".format(str(grp),self.reverse_heading_map[grp]))
				(l_orig,r_orig,t_orig,text_orig)=heading_grp_extracted.get(self.reverse_heading_map[grp])
				l_new,r_new = val['left'], val['right']
				heading_grp_extracted[self.reverse_heading_map[grp]] = (l_new,r_new,head_top_min,head_bottom_max,text_orig)
				if self.debug: print("\tUpdated from --> to\n\t\tleft: {:>6}-->{:>6}\tright: {:>6}-->{:>6}\n\t\ttop: {:>7}-->{:>6}\tbottom:  {:>5}\n".format(str(l_orig),str(l_new),str(r_orig),str(r_new),str(t_orig),str(head_top_min),str(head_bottom_max)))
			else:
				text = 'unknown_'+str(grp) #unknown heading name (new)
				if self.debug: print("Creating entry for group: {:^6}, named: {:>17}".format(str(grp),text))
				grp_left_ll = min(list(df[df['grp_match_new'] == grp].left)) #find left min value for group
				grp_right_ll = max(list(df[df['grp_match_new'] == grp].right)) #find right max value for group
				heading_grp_extracted[text] = (grp_left_ll,grp_right_ll,head_top_min,head_bottom_max,text) ##Create new entry in dict

				self.reverse_heading_map[grp] = text ##Also made additional entry in reverse mapping of group with name

				if self.debug: print("\tcreated for group with:\n\t\tleft: {:>6}\tright: {:>6}\n\t\ttop: {:>7}\tbottom: {:>5}\n".format(str(grp_left_ll),str(grp_right_ll),str(head_top_min),str(head_bottom_max)))
		return heading_grp_extracted


	def find_grp_using_logic(self,df,heading_grp_extracted,heading_line,stop_line,roi_min_line):

		#For finding the group first thing it needs to do is set the Heading group found earlier in it's calculation
		set_headings = self.set_heading_grp_found_earlier_in_class(heading_grp_extracted)
		if set_headings and self.debug: print("Headings group set in class memory successfully")

		#Main logic starts here
		if heading_line != 999 and len(self.reverse_heading_map) >0: #case when we have some headings identified
			df['grp_match_new'] = df.apply(lambda row: self.calc_vertical_group(row,heading_line,stop_line,True),axis=1)
			if len(self.glb_grp_chng) != 0 :
				for k,v in self.glb_grp_chng.items():
					df['grp_match_new'] = df['grp_match_new'].replace(k, v)
		elif heading_line != 999 :  #case where headings are not found so no heading processing done #need to further analyze
			df['grp_match_new'] = df.apply(lambda row: self.calc_vertical_group(row,heading_line,stop_line,False),axis=1)
			if len(self.glb_grp_chng) != 0 :
				for k,v in self.glb_grp_chng.items():
					df['grp_match_new'] = df['grp_match_new'].replace(k, v)
		else:
			print("Not able to identify proper Format from Image please try other image")
			raise Exception("Process Aborted, Cannot extract data from Image")

		############## Find any extra group(other than in heading) found during closeness call ##################
		#############  Also update their pixel values based on new found  #######################################

		#unique_grp_identified = list(df['grp_match_new'].unique())
		#heading_grp_we_found = [v for v in self.reverse_heading_map]
		#extra_groups_identified = [k for k in unique_grp_identified if k not in heading_grp_we_found and k!= 999]

		heading_grp_extracted = self.find_nd_upd_limits_of_grp_identified_in_df(df,heading_grp_extracted,roi_min_line)

		return df,heading_grp_extracted,self.reverse_heading_map


################################################################################
################################################################################

class create_tabular_df(object):
	########################### STEP10: Create a Pandas Dataframe(with columns names as table)
	'''
	LOGIC to create dataframe with column names out of processed and extracted dataframe we have
	'''
	def __find_group_which_is_on_most_extreme_left(self,df_filt,reverse_heading_map_1):
		'''
		This method will find the left most group identified on our df.
		By knowing this we can assume that whebever this group comes in dataframe then new line should be initialized
		'''
		min_grp_dic = {}
		for grp in reverse_heading_map_1:
			try:
				min_grp_dic[grp] = min(list(df_filt[df_filt['grp_match_new'] == grp].left))
			except:
				print("No body entry for this group:",reverse_heading_map_1[grp])
		return min(min_grp_dic, key=min_grp_dic.get)


	def __extract_infmn_nd_assign_to_groups(self,line_df,reverse_heading_map_1):
		'''
		This method will create dictionary with all groups identified as keys and if any value present in current line then assign
		otherwise leave it blank
		'''
		temp_df = {}
		for k in reverse_heading_map_1:
			temp_df[k] = None

		for index, row in line_df.iterrows(): #Loop for all rows in Subset of Single line only
			temp_df[row['grp_match_new']] = row['text']
		return temp_df

	###############

	def create_dict_with_group_and_text_also_merge_multilines_based_on_leftmost_grp(self,df, reverse_heading_map_1,sorted_head):
		##Take only those rows which have identified to be in some group
		df_filt = df[df['grp_match_new'].isin([k for k in reverse_heading_map_1])]

		## This group will be used to check for new line always
		#left_most_group = self.__find_group_which_is_on_most_extreme_left(df_filt,reverse_heading_map_1)
		left_most_group =  [k for k,v in reverse_heading_map_1.items() if v==sorted_head[0]][0]

		all_lines = df_filt.line_num.unique()

		prev_line = all_lines[0] #initialized prev_lines as first line for processing any merge later
		combined_line_dict = {}

		if len(sorted_head) >= 3:
			last_grp = [k for k,v in reverse_heading_map_1.items() if v==sorted_head[-1]][0]
			second_lst_grp = [k for k,v in reverse_heading_map_1.items() if v==sorted_head[-2]][0]

			for line in all_lines:
				if line == all_lines[0]: #it means first line and can have no Date column
					line_df = df_filt[df_filt['line_num'] == line]
					ext_line_dict = self.__extract_infmn_nd_assign_to_groups(line_df,reverse_heading_map_1)
					combined_line_dict[line] = ext_line_dict
				else:
					line_df = df_filt[df_filt['line_num'] == line] #extract dataframe for current line
					'''
					#means should be new line by check if left most group is in line
					'''
					if (left_most_group in list(line_df.grp_match_new)) and (last_grp in list(line_df.grp_match_new) or second_lst_grp in list(line_df.grp_match_new)):
						prev_line = line #change for ongoing line to current line
						ext_line_dict = self.__extract_infmn_nd_assign_to_groups(line_df,reverse_heading_map_1)
						combined_line_dict[prev_line] = ext_line_dict
						'''
					It means need to be merge with previous line
					'''
					else: #means no first left column and should be merged with previous line dict
						prev_line_dict = combined_line_dict[prev_line]
						ext_line_dict = self.__extract_infmn_nd_assign_to_groups(line_df,reverse_heading_map_1)
						for key in prev_line_dict:
							if prev_line_dict.get(key,None):
								if ext_line_dict.get(key,None):
									prev_line_dict[key] = prev_line_dict.get(key)+' '+ext_line_dict.get(key)
						combined_line_dict[prev_line] = prev_line_dict

		else:
			print("Not able to found enough columns to process logic of last columns check")
			for line in all_lines:
				if line == all_lines[0]: #it means first line and can have no Date column
					line_df = df_filt[df_filt['line_num'] == line]
					ext_line_dict = self.__extract_infmn_nd_assign_to_groups(line_df,reverse_heading_map_1)
					combined_line_dict[line] = ext_line_dict
				else:
					line_df = df_filt[df_filt['line_num'] == line] #extract dataframe for current line

					'''
					#means should be new line by check if left most group is in line
					'''
					if left_most_group in list(line_df.grp_match_new):
						prev_line = line #change for ongoing line to current line
						ext_line_dict = self.__extract_infmn_nd_assign_to_groups(line_df,reverse_heading_map_1)
						combined_line_dict[prev_line] = ext_line_dict
						'''
					It means need to be merge with previous line
					'''
					else: #means no first left column and should be merged with previous line dict
						prev_line_dict = combined_line_dict[prev_line]
						ext_line_dict = self.__extract_infmn_nd_assign_to_groups(line_df,reverse_heading_map_1)
						for key in prev_line_dict:
							if prev_line_dict.get(key,None):
								if ext_line_dict.get(key,None):
									prev_line_dict[key] = prev_line_dict.get(key)+' '+ext_line_dict.get(key)
						combined_line_dict[prev_line] = prev_line_dict

		return combined_line_dict

	def create_pandas_df_from_dict(self,df,reverse_heading_map,sorted_head):

		##STEP1 :: Extract all details and merge any plurist line if any
		combine_line_dict_1 = self.create_dict_with_group_and_text_also_merge_multilines_based_on_leftmost_grp(df,reverse_heading_map,sorted_head)
		## Step2:: Create a dataframe with column names out of dict
		temp_list = []
		for line,v in combine_line_dict_1.items():
			temp_dict = {}
			temp_dict['line_num'] = line
			for grp,text in v.items():
				temp_dict[reverse_heading_map[grp]] = text
			temp_list.append(temp_dict)


		return pd.DataFrame(temp_list).reindex(columns=sorted_head)

################################################################################
######################### STEP: Changes in to send back dictionary #############
def find_body_grp_dict(df,all_heading_grp_dict_1,roi_min_line,roi_stop_line):
	'''
	This method will change the all headings detail dictionary and impute additional detail with body info.
	Earlier:
		all_heading_grp_dict = {'BALANCE': (1879, 1994, 1051, 'BALANCE'),
												  DATE': (26, 89, 1054, 'DATE'),
												  DATE1': (132, 260, 997, 'VAL'),
				    							  DEBIT_CREDIT': (1371, 1610, 1053, 'DEBET CREDIT'),
												  DESCRIPTION': (516, 1082, 1052, 'REFERENCE DESCRIPTION')}
	NEW:
		all_heading_grp_dict = {'BALANCE': {'head':(1879, 1994, 1051, 1251, 'BALANCE'),
																  'body':(1879, 1994, 1253, 1765)	},
												'DATE': {...
																...													},
												...
												...
											}
	'''
	body_top_pixel = min(list(df[df['line_num']==roi_min_line].top))
	body_bottom_pixel = min(list(df[df['line_num']==roi_stop_line].top))-5

	for grp,v in all_heading_grp_dict_1.items():
		grp_left = v[0]
		grp_right = v[1]

		body = (grp_left,grp_right,body_top_pixel,body_bottom_pixel)
		all_heading_grp_dict_1[grp] = {'body':body,'head':v}
	return all_heading_grp_dict_1

################################################################################
######################## STEP: Merge some groups which are close to each other.
def find_very_near_group_to_merge(all_heading_grp_dict,ratio_1 = 46,ratio_2 = 2.8):
	'''
	ratio_1 calc:
		(a) - total_width = 3217  -- abs_dif
		(b) - seen min in between group(named group) = 70  ##Heuristic
			ratio_1 = (a)/(b) = 3217/70  = ~46
				So if (a)/ratio_1 = (b)

		(c) - seen betweem known and unknown group max dist. = 20  ##Heuristic, but we want to determine this
			ratio_2 = (b)/(c)  = 70/25 = ~2.8
			so if (b)/ratio_2  = (c) can be determined.
	'''

	#### Find dimensions required

	grp_dict_l = {grp:sett[0] for grp,sett in all_heading_grp_dict.items()}
	grp_dict_r = {grp:sett[1] for grp,sett in all_heading_grp_dict.items()}
	sorted_head = sorted(grp_dict_l, key=grp_dict_l.__getitem__) #Sorted heading based on Left pixel

	left_most = all_heading_grp_dict[sorted_head[0]][0]
	right_most = all_heading_grp_dict[sorted_head[-1]][1]
	abs_dif = abs(right_most-left_most) ##Pixel Area between First group and Last group - (a)

	max_sep_val = (abs_dif/ratio_1)/ratio_2 ##Find the distance based on area and ratio under which any group will be send to merge

	#### Logic to find any successive group if have value less than max_sep_val
	to_merge = []
	to_skip=False
	for i,grp in enumerate(sorted_head):
		if i < len(sorted_head)-1:
			if to_skip:
				to_skip=False
				continue
			if (re.search('unknown', grp) and not re.search('unknown', sorted_head[i+1])) or (not re.search('unknown', grp) and re.search('unknown', sorted_head[i+1])):
				if abs(grp_dict_r[sorted_head[i]]-grp_dict_l[sorted_head[i+1]]) < max_sep_val:
					to_skip = True
					to_merge.append((sorted_head[i],sorted_head[i+1]))
	#sorted_head_dict = {v:i+1 for i,v in enumerate(sorted_head)}
	return to_merge,sorted_head


#proc_df_cp = proc_df.copy()
#proc_df = proc_df_cp

def replace_merge_heading_found(proc_df,to_merge,reverse_all_heading_grp_map,all_heading_grp_dict,sorted_head):
	#### Now check if unknown headings should be there always(both known heading means something is wrong or no need to merge.)
	for grp_swop in to_merge:
		grp_to = grp_swop[0]
		grp_frm = grp_swop[1]

		if re.search('unknown', grp_swop[0]) or re.search('unknown', grp_swop[1]): #only proceed if any unknown present
			if re.search('unknown', grp_swop[0]):
				grp_to = grp_swop[1]
				grp_frm = grp_swop[0]
			else: ## either unknown present in second position or unknown present in both position- doesn't matter
				grp_to = grp_swop[0]
				grp_frm = grp_swop[1]

			##Find group number for this heading
			grp_to_num = [k for k,v in reverse_all_heading_grp_map.items() if v==grp_to][0]
			grp_frm_num = [k for k,v in reverse_all_heading_grp_map.items() if v==grp_frm][0]

			########## Change in Dataframe
			proc_df['grp_match_new'] = proc_df['grp_match_new'].replace(grp_frm_num,grp_to_num)

			########## Change in reverse mapping
			del reverse_all_heading_grp_map[grp_frm_num]

			########## Change in all_dict pixel values
			(to_left,to_right,to_top,to_bot,to_text) = all_heading_grp_dict[grp_to]
			(frm_left,frm_right,frm_top,frm_bot,frm_text) = all_heading_grp_dict[grp_frm]
			all_heading_grp_dict[grp_to] = (min(to_left,frm_left),max(to_right,frm_right),to_top,to_bot,to_text)
			del all_heading_grp_dict[grp_frm]

			########## Delete group entry from the sorted list also
			sorted_head= [grp for grp in sorted_head if grp!= grp_frm]


	return proc_df,reverse_all_heading_grp_map,all_heading_grp_dict,sorted_head





################################################################################
##################  get current timestamp in format for logging purpose
def cur_time(arg1):
	return datetime.datetime.now().strftime(arg1)

def main(image_path,debug=False):

	print("Reading Image Starting-"+cur_time("%H:%M:%S")+'\n')
	create_df = initial_read_bank_stmt(image_path,debug) #initialize the class with image_path provided
	raw_df = create_df.main_till_df() #it will do ocr and merge close words and return pandas df.
	print("\tFinished")

	print("Finding ROI and heading Starting-"+cur_time("%H:%M:%S")+'\n')
	roi = find_ROI(debug=False)
	heading_line,roi_min_line,roi_stop_line,heading_grp_extracted = roi.main_ROI_region(raw_df)
	print("\tFinished")

	print("Grouping in ROI Starting-"+cur_time("%H:%M:%S")+'\n')
	roi_grp = closeness_new(debug=False)
	proc_df,all_heading_grp_dict,reverse_all_heading_grp_map = roi_grp.find_grp_using_logic(raw_df,heading_grp_extracted, heading_line,roi_stop_line,roi_min_line)
	print("\tFinished")

	print("Merging any close columns staretd-"+cur_time("%H:%M:%S")+'\n')
	to_merge,sorted_head = find_very_near_group_to_merge(all_heading_grp_dict)
	proc_df,reverse_all_heading_grp_map,all_heading_grp_dict,sorted_head =  replace_merge_heading_found(proc_df,to_merge, reverse_all_heading_grp_map,all_heading_grp_dict,sorted_head)
	print("\tFinished")

	print("Creating tabular df Starting-"+cur_time("%H:%M:%S")+'\n')
	create_tab = create_tabular_df()
	tab_df = create_tab.create_pandas_df_from_dict(proc_df,reverse_all_heading_grp_map,sorted_head)
	print("\tFinished")

	all_heading_grp_dict = find_body_grp_dict(raw_df,all_heading_grp_dict,roi_min_line,roi_stop_line)

	return tab_df,all_heading_grp_dict

################################################################################
################################################################################
def plot_borders_on_image(image_path,dic):
	img = cv2.imread(image_path)
	for grp,v in dic.items():
		for area,pixel in v.items():
			left,right,top,bot = pixel[0],pixel[1],pixel[2],pixel[3]
			if area == 'top' or area == 'head':
				left_text = left
				cv2.putText(img,grp, (left_text,top-3), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,0),3)
				cv2.rectangle(img, (left, top), (right, bot), (0,255,0), 2)
			else:
				cv2.rectangle(img, (left, top), (right, bot), (255,0,0), 2)
	plt.imsave('img.jpg', img)


if __name__ == '__main__':
	#image_path = "MANDIRI/MANDIRI AUGUST 18 REDMEN_1.jpg"
	#image_path = "MANDIRI/MANDIRI OCTOBER 18 CHAKRA_1.jpg"
	#image_path = "MANDIRI/MANDIRI OCTOBER 18 CHAKRA_2.jpg"
	#image_path = "MANDIRI/MANDIRI SEPTEMBER 18 TALAGA_1.jpg" #under heading text is not falling inside
	image_path = "DONAMON/DANAMON MAY 18 DCU_1.JPG"   #No heading found
	image_path = "BCA/BCA APRIL 2018 MCM_1.JPG"
	image_path = "BCA/BCA JUNE 2018 ELZIO_1.JPG"
	image_path = "DONAMON/don_wrd_2.jpg"

	#df,grp_dict = main(image_path,debug = True) #To see all print command
	df,grp_dict = main(image_path)
	#Save the Image masked
	status = plot_borders_on_image(image_path,grp_dict)














#########################################################################################
	grp_dict_cp = grp_dict.copy()

	head = [k for k in grp_dict]
	to_merge = []
	for i,heading in enumerate(head):
		if i < len(head)-1:
			if re.search('unknown', heading):
				left_unkn = grp_dict[heading]['head'][0]
				right_unkn = grp_dict[heading]['head'][1]
				for j in range(i+1,len(head)):
					if not re.search('unknown', heading):
						left_inner = grp_dict[head[j]]['head'][0]
						right_inner = grp_dict[head[j]]['head'][1]


						if left_outer<=left_inner<=right_outer or left_outer<=right_inner<=right_outer or left_inner<=left_outer<=right_inner:
							to_merge.append((head[i],head[j]))

	head_sure = [k for k in grp_dict if not re.search('unknown', k)]

import operator
x = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
sorted_x = sorted(grp_dict.items(), key=operator.itemgetter(1))

sorted(grp_dict.items(), key=sorted(operator.itemgetter(), key=operator.itemgetter(1)))

grp_dict_l = {grp:sett[0] for grp,v in grp_dict.items() for typ,sett in v.items()}
grp_dict_r = {grp:sett[1] for grp,v in grp_dict.items() for typ,sett in v.items()}
sorted_head = sorted(grp_dict_l, key=grp_dict_l.__getitem__)

left_most = grp_dict[sorted_head[0]]['body'][0]
right_most = grp_dict[sorted_head[-1]]['body'][1]

abs_dif = abs(right_most-left_most)
'''
ratio_1 calc:
	(a) - total_width = 3217
	(b) - seen min in between group(named group) = 70  ##Heuristic
	ratio_1 = (a)/(b) = 3217/70  = ~46
		So if (a)/ratio_1 = (b)

	(c) - seen betweem known and unknown group max dist. = 20  ##Heuristic, but we want to determine this
	ratio_2 = (b)/(c)  = 70/25 = ~2.8
		so if (b)/ratio_2  = (c) can be determined.
'''
#abs_dif =2500
ratio_1 = 46
ratio_2 = 2.8

max_sep_val = (abs_dif/ratio_1)/ratio_2

to_merge = []
to_skip=False
for i,grp in enumerate(sorted_head):
	if i < len(sorted_head)-1:
		if to_skip:
			to_skip=False
			continue
		if (re.search('unknown', grp) and not re.search('unknown', sorted_head[i+1])) or (not re.search('unknown', grp) and re.search('unknown', sorted_head[i+1])):
			if abs(grp_dict_r[sorted_head[i]]-grp_dict_l[sorted_head[i+1]]) < max_sep_val:
				to_skip = True
				to_merge.append((sorted_head[i],sorted_head[i+1]))




'''
	print("Checking in heading if any other heading pixels consides\n-------------------------------------------------")
	head = [k for k in heading_grp_extracted]
	to_merge = []
	for i,heading in enumerate(head):
		if i < len(head)-1:
			left_outer = heading_grp_extracted[heading][0]
			right_outer = heading_grp_extracted[heading][1]
			print("\nfor head: {:10s} \t left:{} \t right:{}".format(heading,left_outer,right_outer))
			for j in range(i+1,len(head)):
				left_inner = heading_grp_extracted[head[j]][0]
				right_inner = heading_grp_extracted[head[j]][1]
				print("\tcheck: {:15s} \t left:{} \t right:{}".format(head[j],left_inner,right_inner))

				if left_outer<=left_inner<=right_outer or left_outer<=right_inner<=right_outer or left_inner<=left_outer<=right_inner:
					to_merge.append((head[i],head[j]))
					print("\t\tHappen Inside, need to replace")
				'''