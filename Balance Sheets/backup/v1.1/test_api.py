# -*- coding: utf-8 -*-
"""
Created on Mon Dec 10 12:20:45 2018

@author: CWDSG05
"""

##Script to test the API
import jsonpickle
import requests


########################### Testing for Balance Sheet
addr = 'http://localhost:5253'
test_url2 = addr + '/api/v1.0/bal_sheet/extract_img'
headers = {'content-type': 'application/json'}

file_name = "fin1_5.jpg"
file_name = "fin1_4.jpg"

#creating payload in Dic format
payload = {'file_path':file_name}
data = jsonpickle.encode(payload)


#sending request to sever
response = requests.post(test_url2, data=data, headers=headers)
response = response.json()


################################################################################
##################### Test 2nd API #############################################
addr = 'http://localhost:5253'
test_url2 = addr+'/api/v1.0/bal_sheet/send_train'
headers = {'content-type': 'application/json'}

#creating payload in Dic format
payload = {'to_train':{'ASSETS_CURRENT':{'BIAYA DIBAYAR DIMUKA':'Payment in Advance'},
								   'ASSETS_NONCURRENT':{'ASET TETAP NILAI BUKU':'Goodwill/net'}
								   }
			}
data = jsonpickle.encode(payload)

#sending request to sever
response = requests.post(test_url2, data=data, headers=headers)
response = response.json()
################################################################################
import pandas as pd
filename = r"C:\financial_scan\scripts\working_all_classes_v1\train_data\test_file.train"
read = pd.read_csv(filename, sep='@')


##function without file write
def predict_template_subcateg(fin_result,model):
	##Below code will go into the dict and for each subcategories under categories of ASSETS,ASSETS_CURRENT,ASSETS_NONCURRENT,LIABILITIES,LIABILITIES_CURRENT,
	# LIABILITIES_NONCURRENT, EQUITY, EQUITY_AND_LIABILITIES
	## It will predict for these subcat into corresponding excel_template matching texts.
	## Once return the result it will replace the keys in dictionary with new found subcategories

	##############################################
	# Change the LIABILITIES_SHORT to CURRENT_LIABILITIES
	# and LIABILITIES_LONG to NONCURRENT_LIABILITIES
	#f.write("\t\t Replacing LIABILITIES_SHORT or LIABILITIES_LONG\n")
	print("\t\t Replacing LIABILITIES_SHORT or LIABILITIES_LONG\n")
	for groups,details in fin_result.items():
		for categ in details:
			if categ =='LIABILITIES_SHORT':
				fin_result[groups]['LIABILITIES_CURRENT'] = fin_result[groups].pop(categ)
			elif categ =='LIABILITIES_LONG':
				fin_result[groups]['LIABILITIES_NONCURRENT'] = fin_result[groups].pop(categ)
	##############################################
	#f.write("\t\t Iterating to predict in dictionary return\n")
	print("\t\t Iterating to predict in dictionary return\n")
	for groups,details in fin_result.items():
		print("\t\t{}".format(groups))
		#f.write("\n\t\t GROUP: {}\n".format(groups))
		print("\n\t\t GROUP: {}\n".format(groups))
		for categ,subcat_det in details.items():
			print("\t\t\t{}".format(categ))
			#f.write("\t\t\t {}\n".format(categ))
			print("\t\t\t {}\n".format(categ))
			x_test = [x for x in subcat_det]
			#f.write("\t\t\t\t Original Tags: {}\n".format(x_test))
			print("\t\t\t\t original tags: {}".format(x_test))

			y_predict =model.predict_subcat(categ,x_test)  #Call for predict here using prebuild models
			if y_predict == 1:
				print(">>>>>>> ERROR <<<<<<<< in Prediction")
				#f.write("\t\t\t\t >>>>> ERROR <<<<<< in prediction, status 1\n")
				print("\t\t\t\t >>>>> ERROR <<<<<< in prediction, status 1\n")
			else:
				print("\t\t\t\tmapped tags: {}".format(y_predict))
				print("-----------------------")
				#f.write("\t\t\t\t mapped Tags: {}\n".format(y_predict))
				print("\t\t\t\t mapped Tags: {}\n".format(y_predict))
				#f.write("\t\t\t\t --------------------------\n")
				print("\t\t\t\t --------------------------\n")

				#fin_result[groups][x for x in y_predict] = fin_result[groups].pop([str(y) for y in x_test])
				for i,j in enumerate(x_test):
					fin_result[groups][categ][y_predict[i]] = fin_result[groups][categ].pop(j)

				#fin_result[groups][x for x in y_predict] = fin_result[groups].pop([str(y) for y in x_test])
	return fin_result

fin_result_mapped = predict_template_subcateg(fin_result,model)

import jsonify

jsonify(jsonpickle.encode({'Extracted_OCR':fin_result_mapped})), 200


jsonpickle.encode({'Extracted_OCR':fin_result_mapped})