# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 16:47:32 2018

@author: CWDSG05
"""

from flask import Flask, jsonify, abort, make_response, request,send_file
import json,os
from numpy import int0
import datetime

os.chdir(r"C:\financial_scan\scripts\working_all_classes_v1")

from script_modules.extract_categories_v1 import convert_ocr
from script_modules.model_build_classify import classify

def cur_time(arg1):
	return datetime.datetime.now().strftime(arg1)

ocr = convert_ocr()
model = classify()

NOT_FOUND = 'Not found'
BAD_REQUEST = 'Bad request'
NO_PDF = 'PDF/Image not found at path'
NO_EXEC_OCR = 'Error while extracting OCR category tags'
NO_EXEC_PREDICT = 'Error while Predicting OCR sub-category tags'

app = Flask(__name__)

@app.errorhandler(404)
def not_found(error):
	return make_response(jsonify({'error': NOT_FOUND}), 404)

@app.errorhandler(400)
def bad_request(error):
	return make_response(jsonify({'error': BAD_REQUEST}), 400)

@app.errorhandler(408)
def pdf_not_found(error):
	return make_response(jsonify({'error': NO_PDF}), 408)

@app.errorhandler(412)
def failed_execution_part1(error):
	return make_response(jsonify({'error': NO_EXEC_OCR}), 412)

@app.errorhandler(416)
def failed_execution_part2(error):
	return make_response(jsonify({'error': NO_EXEC_PREDICT}), 412)

@app.route('/')
def hello_world():
	return jsonify({'Balance_sheet OCR': 'server is active'}), 200

######################################### DEFINE THE FUNCTIONS WE GONNA USE FOR API USAGE ######################################
################################################################################################################################

def predict_template_subcateg(fin_result,f,model):
	##Below code will go into the dict and for each subcategories under categories of ASSETS,ASSETS_CURRENT,ASSETS_NONCURRENT,LIABILITIES,LIABILITIES_CURRENT,
	# LIABILITIES_NONCURRENT, EQUITY, EQUITY_AND_LIABILITIES
	## It will predict for these subcat into corresponding excel_template matching texts.
	## Once return the result it will replace the keys in dictionary with new found subcategories

	##############################################
	# Change the LIABILITIES_SHORT to CURRENT_LIABILITIES
	# and LIABILITIES_LONG to NONCURRENT_LIABILITIES
	f.write("\t\t Replacing LIABILITIES_SHORT or LIABILITIES_LONG\n")
	for groups,details in fin_result.items():
		for categ in details:
			if categ =='LIABILITIES_SHORT':
				fin_result[groups]['LIABILITIES_CURRENT'] = fin_result[groups].pop(categ)
			elif categ =='LIABILITIES_LONG':
				fin_result[groups]['LIABILITIES_NONCURRENT'] = fin_result[groups].pop(categ)
	##############################################
	f.write("\t\t Iterating to predict in dictionary return\n")
	for groups,details in fin_result.items():
		print("\t\t{}".format(groups))
		f.write("\n\t\t GROUP: {}\n".format(groups))
		for categ,subcat_det in details.items():
			print("\t\t\t{}".format(categ))
			f.write("\t\t\t {}\n".format(categ))
			x_test = [x for x in subcat_det]
			print("\t\t\t\t original tags: {}".format(x_test))
			f.write("\t\t\t\t Original Tags: {}\n".format(x_test))
			y_predict =model.predict_subcat(categ,x_test)  #Call for predict here using prebuild models
			if y_predict == 1:
				print(">>>>>>> ERROR <<<<<<<< in Prediction")
				f.write("\t\t\t\t >>>>> ERROR <<<<<< in prediction, status 1\n")
			else:
				print("\t\t\t\tmapped tags: {}".format(y_predict))
				print("-----------------------")
				f.write("\t\t\t\t mapped Tags: {}\n".format(y_predict))
				f.write("\t\t\t\t --------------------------\n")

				#fin_result[groups][x for x in y_predict] = fin_result[groups].pop([str(y) for y in x_test])
				for i,j in enumerate(x_test):
					fin_result[groups][categ][y_predict[i]] = fin_result[groups][categ].pop(j)

				#fin_result[groups][x for x in y_predict] = fin_result[groups].pop([str(y) for y in x_test])
	return fin_result

def train_model(categ):
	if model.train_naive_bayes(categ):
		print("\t\t\t Model for {} trained successfull\n".format(categ))
		return True
	else:
		print("\t\t\t Training model >>>>>FAIL<<<< for {}, Check the Training file\n".format(categ))
		return False
################################################################################################################################
################################################################################################################################

@app.route('/api/v1.0/bal_sheet/extract_img', methods=['POST','GET'])
def receive_sheet_image_name_and_return_extracted_categ_unmatchedTags():

	with app.app_context():
		if not request.json or 'file_path' not in request.json:
			print("Bad request received::")
			print(request.data)
			abort(400)

		print("request received :{}\n".format(request.json))
		output_img = request.json['file_path'] 	#extract image path from dictionary

		if not os.path.isfile(output_img):
			print("File is not present")
			abort(408)
		try:
			(fin_result,not_matched,logname) = ocr.call_extract_categories(output_img)
			print("Part1 completed")
		except:
			print("Part1 Failed")
			abort(412)

		################Calling part 2 for predict of found subcategories inside categories
		with open(logname,'a+') as f:
			f.write("\n========== OCR extraction Finish: {} ===================\n".format(cur_time("%H:%M:%S")))
			try:
				fin_result_mapped = predict_template_subcateg(fin_result,f,model)
				f.write("\n========== Prediction  call Finish: {} ===================\n".format(cur_time("%H:%M:%S")))
			except:
				f.write("\n========== Prediction  call >>>>FAILED<<<< : {} ===================\n".format(cur_time("%H:%M:%S")))
				print("Part2 Failed")
				abort(416)

		return jsonify({'Extracted_OCR': str(fin_result_mapped), 'NOT_MAPPED':str(not_matched)}), 200
		#return jsonify({'Extracted_OCR': fin_result_mapped, 'NOT_MAPPED':not_matched}), 200

######################@nd Endpoint for saving new label
@app.route('/api/v1.0/bal_sheet/send_train', methods=['POST','GET'])
def receive_new_train_data_with_subcategory_and_need_to_retrain():
	'''
	Received json should be in below format::

		{'to_train':{'ASSETS_CURRENT':{'BIAYA DIBAYAR DIMUKA':'Payment in Advance'},
								   'ASSETS_NONCURRENT':{'ASET TETAP NILAI BUKU':'Goodwill, net'}
							   }
			}

	{to_train: { category : {'text in bahasa': 'already known subcategory'}
						}
	}
	'''

	if not request.json or 'to_train' not in request.json:
		print("Bad request received::")
		print(request.data)
		abort(400)
	print("request received :{}\n".format(request.json))
	retraining_text = request.json['to_train'] 	#extract training dict from big dictionary

	try:
		for categ,subcat_dict in retraining_text.items():
			categ = categ.upper()
			filename = "train_data/"+categ+"_newtrain.csv"
			if not os.path.isfile(filename): #means file is not present before
				with open(filename,'a+') as file:
					file.write("text,label\n")
					for bahasa_ocr,to_map_subcat in subcat_dict.items():
						bahasa_ocr = bahasa_ocr.upper()
						to_map_subcat = to_map_subcat.upper()
						mapped_number_frm_to_map = model.extract_key_from_value_self(categ,to_map_subcat)
						if mapped_number_frm_to_map != 99:
							file.write("{},{}\n".format(bahasa_ocr,mapped_number_frm_to_map)) ##might need to get here mapping number for to_map string from model_build_classify.py
						else:
							text = "Not found match for: "+to_map_subcat+" under: "+categ
							return jsonify({'error': text}), 420
					print("Sent for training: File:{}, original text:{}, Mapped subCategory:{}".format(categ+"_newtrain.csv",bahasa_ocr,mapped_number_frm_to_map))
			else:
				with open(filename,'a+') as file:
					for bahasa_ocr,to_map_subcat in subcat_dict.items():
						bahasa_ocr = bahasa_ocr.upper()
						to_map_subcat = to_map_subcat.upper()
						mapped_number_frm_to_map = model.extract_key_from_value_self(categ,to_map_subcat)
						if mapped_number_frm_to_map != 99:
							file.write("{},{}\n".format(bahasa_ocr,mapped_number_frm_to_map)) ##might need to get here mapping number for to_map string from model_build_classify.py
						else:
							text = "Not found match for: "+to_map_subcat+" under: "+categ
							return jsonify({'error': text}), 420
					print("Sent for training: File:{}, original text:{}, Mapped subCategory:{}".format(categ+"_newtrain.csv",bahasa_ocr,mapped_number_frm_to_map))
		return jsonify({'Status': 'SAVED'}), 200
	except:
		return make_response(jsonify({'error': 'Error while Saving'}), 420)



############Run the app in the last
app.run(host='0.0.0.0',port=5253,debug=True)


#if __name__ == '__main__':


	#output_img = "fin1_5.jpg"

#	app.run(host='0.0.0.0',port=5253,debug=True)

model.extract_key_from_value_self('EQUITY',"PREFERRED STOCKq")