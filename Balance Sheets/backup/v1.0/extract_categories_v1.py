# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 10:46:18 2018

@author: CWDSG05
"""
import os
os.chdir(r"C:\financial_scan")

from pdf2jpg import pdf2jpg
import cv2, string
from pytesseract import pytesseract as pt
from PIL import Image
import pandas as pd
import numpy as np

import pandasql as ps
from difflib import get_close_matches

import jsonpickle, requests
import time, datetime
#import pickle

#STEP4**################################################################################
################################################################################
class closeness(object):
	'''
	This class will try to assign groups to the text using coordinates of box with x-axis(vertical check)
	Primarily check for 3 conditions;
		i)  - Left Pixels are aligned
		ii) - Right pixels are aligned
		iii)- if not both then center of text box aligned

	'''
	def __init__(self):
		self.blk_n = 0
		self.grp ={}
		self.grp_num =1
		self.found_starting_word = False
		self.starting_wrds = ["ASET","LIABILITAS","EKUITAS","ASSETS","LIABILITY","EQUITY"]

	def check_closeness(self,left_in,right_in,buffer = 22,debug=False):
		'''use external dictionary "grp", which will be in format
			grp[grp_num] = {'left' : left, 'right' : right}

			return possible_grp = {'distance':22, 'outlier':0}
						 expansion = {'left':10,'right':0}
		'''
		possible_grp ={}
		#expansion_left = {}
		#expansion_right = {}
		#expansion = {0:{'left':0,'right':0}}
		centre_in = (left_in+right_in)/2

		#print("Input left right from df : left:{}, right:{}".format(left_in,right_in))
		for k,v in self.grp.items():
			right_grp = v['right']
			left_grp = v['left']
			#width_grp = right_grp-left_grp
			centre_grp = (right_grp + left_grp)/2

			#print("....Comparing with current groups, left:{},right:{}".format(left_grp,right_grp))

			#################****Check right axis matching
			if abs(right_grp - right_in) < buffer:
				#print("........inside right side check confirm")
				possible_grp[k] = {'distance':abs(right_grp-right_in),'method':'right'} #distance see between right edges only

			#################****check left axis matching
			elif abs(left_grp - left_in) < buffer:
				possible_grp[k] = {'distance':abs(left_grp-left_in),'method':'left'} #distance will be seen between left edges only
				#print("........inside left side check confirm")

			#################****Check centre aligns
			elif abs(centre_grp-centre_in) < buffer:
				possible_grp[k] = {'distance':abs(centre_grp-centre_in),'method':'centre'} #centre closeness will be monitored
				#print("........inside centre check confirm")

		#print("Possible group")
		#print(possible_grp)
		#print("---------------")
		return possible_grp

	def find_closest_among_possible_groups(self,poss_grp):
		'''
		sorted(poss_grp.items(), key=lambda kv: kv[1]['distance']) will sort based on key 'distance' inside dictionary
		----> [(0, {'distance': 11, 'method': 'right'}),
				   1, {'distance': 9, 'method': 'left'})]
		[0] is added to get top element among sorted list  ---> (1, {'distance': 9, 'method': 'left'})
		[0] is added to get the first element(i.e group number) among the top element tuple ---> 1
		'''
		return sorted(poss_grp.items(), key=lambda kv: kv[1]['distance'])[0][0]

	def calc_vertical_group(self,row):
		'''
		Give grouping number to each numeric entry in dataframe.

		First numeric will be started with 1 and if subsequent any numeric value falls within range of this group
		, this group number  will be assigned to that entry
		else, New group is identified and continue for all
		'''
		if row['numeric'] == True and self.found_starting_word == True: #only check for numeric records and if starting_words found already
			left = row['left']
			right = row['right']
			#blok = row['block_num']
			#lin = row['line_num']
			#wor = row['word_num']
			if self.blk_n == 0: #means first entry
				#self.blk_n = row['block_num'] #set current block number as blk_n
				#grp[grp_num] = [(left,right),(blok,lin,wor),(1,1)] #initiate the dictionary
				self.grp[self.grp_num] = {'left' : left, 'right' : right, 'count':1} #initiate the dict with left,right and count in this group
				self.blk_n =1
				return self.grp_num
			else:
				poss_grp = self.check_closeness(left,right) #if it's not first numeric element then check closeness of this block with all previous groups identified in 'grp'
				#if not(not poss_grp): #if something present in possible group then continue
				#	for k,v in poss_grp.items():
			#print(self.grp)
				if len(poss_grp)>1:
					#multiple groups identified for this row need to create logic
					#grp_found = list(poss_grp.keys())
					return self.find_closest_among_possible_groups(poss_grp) #return the group identified by smallest distance if more than 1 identified
				elif len(poss_grp)==1:
					#only 1 group identified and can be used directly
					grp_found = int(list(poss_grp.keys())[0])
					grp_found_cnt_before = self.grp[grp_found]['count']
					self.grp[grp_found]['count'] = grp_found_cnt_before+1
					return grp_found #return single group identified
				else:
					#nothing returned from current group so can be contender for new group
					self.grp_num += 1 #increment the group to be assigned
					self.grp[self.grp_num] = {'left':left,'right':right,'count':1} #initiate the new group identified
					return self.grp_num
		else:
			if self.found_starting_word ==False and len(get_close_matches(row['text'],self.starting_wrds, 1, 0.70))>0: #check if current non-numeric belongs to starting words identified to start counting for grouping
				self.found_starting_word = True #starting word now found and grouping can be started
			return 0 ###Default group for non-numeric (In our case marking unimportant for calculations)



class convert_ocr(object):

	def __init__(self):
		#self.n = 10
		self.p = closeness()

	#STEP1**##########################***First convert PDF to Image***######################
	################################################################################
	def convert_pdf_2_image(self,input_pdf,pages='0'):
		'''Take input pdf name and pages in string
		Return path of image file created
		'''
		inputpath = input_pdf
		outputpath = os.path.dirname(inputpath)+"\pdf2jpg"
		result = pdf2jpg.convert_pdf2jpg(inputpath, outputpath, pages=pages)
		return result[0]['output_jpgfiles'][0]

		'''
		# To convert multiple pages
		result = pdf2jpg.convert_pdf2jpg(inputpath, outputpath, pages="1,0,3")
		print(result)

		# to convert all pages
		result = pdf2jpg.convert_pdf2jpg(inputpath, outputpath, pages="ALL")
		print(result)
		'''

	def convert_pdf2Image_in_R(self,pdf_name,pages,dpi=300):
		'''
		Perform Image conversion in R server, for better result and flexibility
		'''
		addr = 'http://localhost:5253'
		test_url2 = addr + '/pdf2image'
		headers = {'content-type': 'application/json'}

		pdf_path = pdf_name

		payload = {'pdf.path':pdf_path,'pages':pages,'dpi':dpi}
		data = jsonpickle.encode(payload)

		response = requests.post(test_url2, data=data, headers=headers)
		response = response.json()
		return response['image_name']

	#STEP2**################################################################################
	################################################################################
	#image_path = 'C:\\financial_scan\\pdf2jpg\\fin1.pdf\\4_fin1.pdf.jpg'

	def perform_ocr_on_image(self,image_path):
		'''
		__takes__ : image path as input
		__provides__: provides raw ocr text parsed with page_num,block_num,Line_num,word_num and box dimensions with text.

		filter words which have less than 55 percent confidence and sort based on block,line and word result df.
		'''
		img = cv2.imread(image_path) ## Read the image
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)     ## converting to grayscale

		##Run tesseract call
		#indo_dat = pt.image_to_data(Image.fromarray(gray),lang='ind',config='--psm 1',output_type=pt.Output.DICT )

		config = ("-l ind --oem 1 --psm 6")
		ocr_raw = pt.image_to_data(Image.fromarray(gray),config= config )
		#ocr_raw = pt.image_to_string(Image.fromarray(gray),config= config )
		#ocr_raw = pt.image_to_data(Image.fromarray(gray),lang='ind',config='--psm 1' )
		return ocr_raw

	def create_dataframe_from_ocr_raw(self,ocr_raw):
		'''
		__takes__: takes raw string text which are \n(newline) separated for  rows and \t(tab) for columns.
		__provides__: pandas dataframe created which have aall columns and rows from raw_ocr

		__columns_created__: "level","page_num	","block_num","par_num","line_num	","word_num","left","top","width","height","conf","text"

		'''
		##Create dataframe from extracted text
		indo_split = ocr_raw.split("\n") #split first based on newline character for different rows

		lis = [] #initiate list for body
		for i,text in enumerate(indo_split):
			if i ==0:
				print(text) #print out the heading only
				heading = text.split("\t") #column separators are \t(tab delimited)
			else:
				row = text.split("\t") ##Return is tab separated
				lis.append(row)

		##Create a dataframe using headings extracted from first row of tesseract call and body as list found
		dataf = pd.DataFrame(lis,columns=heading)

		##Trying to convert all possible column to numeric type if not possible ignore
		dataf = dataf.apply(pd.to_numeric, errors='ignore')


		dataf_filtered = dataf.query('conf>42')

		## Sort the dataframe based on Blocks then line in blocks then words in line.
		dataf_filtered = dataf_filtered.sort_values(['block_num', 'line_num','word_num'], ascending=[True, True, True])

		return dataf_filtered



	#STEP3**################################################################################
	################################################################################
	##########Calculate 4 new columns in dataframe for right,center,numeric and text_conv
	def calc_right(self,row):
		return row['left']+row['width']

	def calc_center(self,row):
		return row['left']+row['width']/2

	def find_numeric(self,row,exclude,debug=False):
		'''
		Finds the numeric type in 'Text' column where ratio between digit type and non-digit > 59%

		'''
		threshold = .59
		text = row['text'] #takes only text column from dataframe
		if debug: print("text input:",text)
		try:
			text_len = len(text) #note the total length of text column
			if debug: print("length identified:",text_len)
			if text_len > 0: #if text column is non-empty
				digit_len = 0 #initiate variable to store digit count in text
				text = ''.join(ch for ch in text if ch not in exclude) #remove any punctuation from exclude list
				if debug: print("text after removing exclude list:",text)
				for ch in text: #after removal of punctuation goes through every letter of text column
					if ch.isdigit(): #check if letter is digit
						digit_len += 1 #increment the count by 1
				if debug: print("digits identified:",digit_len)
				ratio = digit_len/text_len #find the ratio of digit to whole text length
				if debug: print("Ratio is:",ratio)
				return True if ratio > threshold else False #only return true when ratio identified is >.59
			return False
		except:
			return False

	def convert_text_to_numeric(self,row):
		'''
		Places where Text is identified as Numeric there correction needs to be done for other characters into numeric
		'''
		exclude = ['.',',','\'']
		num_fix = {'B': '8', 'C': '0', 'D': '0', 'G': '6', 'I': '1', 'O': '0', 'Q': '0', 'S': '5', 'Z': '2', 'U':'0',']':'1','[':'1','!':'1'}

		text = row['text'] #takes only text column from dataframe
		if row['numeric'] == True:
			text_correct = ''.join(num_fix.get(ch, ch) for ch in text if ch not in exclude)
		else:
			text_correct = text
		return text_correct

	def create_derived_cols(self,ocr_df_in,exclude):
		'''
		Create 4 additional derivated columns in df i.e:
			  i)- Right coordinate of word
			 ii)- Center coordinate of word box
			iii)- Text contains numeric content or not(ratio between number and non-number in text shud be >.59 to be True)
			 iv)- Try to correct the numeric identified rows text to Numeric if any other identified characters.
		'''
		ocr_df_in['right'] = ocr_df_in.apply (lambda row: self.calc_right(row),axis=1)
		ocr_df_in['center'] = ocr_df_in.apply (lambda row: self.calc_center(row),axis=1)
		ocr_df_in['numeric'] = ocr_df_in.apply (lambda row: self.find_numeric(row,exclude,True),axis=1)
		ocr_df_in['text_conv'] = ocr_df_in.apply (lambda row: self.convert_text_to_numeric(row),axis=1)

		#convert text_conv column to uppercase
		ocr_df_in['text_conv'] = ocr_df_in['text_conv'].str.upper()

		return ocr_df_in



	#STEP5**################################################################################
	################################################################################
	def find_silhoutte(self,list_in):
		'''
		Function written to find the Silhoutte distance within the list
		Like Scree plot elbow find
		'''
		len_list_in = len(list_in)
		list_out = [0]*len_list_in #of same size as list_in
		for i,ch in enumerate(list_in): #iterate for all elements in Input List
			#print(i)
			if i == len_list_in-1:
				#a= list_in[i]
				#b= 0 #final group size will be 0 only
				#print("comparing:{} and {}".format(a,b))
				#arr[i] = np.linalg.norm(a-b)
				list_out[i] = 0 #putting final entry as 0 distance
			else:
				a= list_in[i] #current
				b= list_in[i+1] #next element
				list_out[i] = np.linalg.norm(a-b)
				print("Distance:: comparing:{} and {} and Silhoutte: {}".format(a,b,list_out[i]))
		return list_out

	def find_cutoff_point(self,grp_min_max):
		'''
		Function will call Silhoutte distance list and then find the maximum value in distance list as Cutoff point
		'''
		import operator

		cnt = list(grp_min_max['count']) #extract all records of count column
		cnt_sil = self.find_silhoutte(cnt) #find distance list from Counts column

		max_index, max_value = max(enumerate(cnt_sil), key=operator.itemgetter(1)) #find maximum value in distance list and index

		cutoff_point = cnt[max_index+1] #Biggest value in distance will be cutoff point

		grp_min_max = grp_min_max[grp_min_max['count']>cutoff_point] #
		numeric_columns_grp = list(grp_min_max['grp'])

		return cutoff_point,numeric_columns_grp

	def remove_oth_grp(self,row,to_keep):
		'''
		Function written to check of grp is in the list=to_keep , then return else return 0
		'''
		if int(row['grp']) in to_keep:
			return row['grp']
		else:
			return 0

	def remove_unwanted_groups_new(self,ocr_df_pls4,debug=True):
		'''
		Method will try to find the groups using elbow method that is where there is maximum dip in the group count, before that thing are the real groups and filter those only
		'''
		q1 = """select count(*) as count,grp from ocr_df_pls4 group by grp""" #find groups and their count
		grp_min_max = ps.sqldf(q1, locals()) #execute the query
		grp_min_max.drop(grp_min_max[grp_min_max.grp == 0].index, inplace=True) #remove 0 group(for text)

		grp_min_max = grp_min_max.sort_values(['count'], ascending=[False]) #Sort the dataframe with group count found for elbow method calculation

		cutoff_point,numeric_columns_grp = self.find_cutoff_point(grp_min_max) #Call function to identify the cutoff point based on elbow methid

		grp_min_max = grp_min_max.sort_values(['grp'], ascending=[True]) #Sort the dataframe with group count found for elbow method calculation

		if debug:
			import matplotlib.pyplot as plt
			plt.plot(grp_min_max) #plot the line chart showing all groups and their count
			plt.axvline(x=cutoff_point)

		grp_min_max = grp_min_max[grp_min_max['count']>cutoff_point] #filter all groups which lies below cutoff point

		to_keep_grp = list(grp_min_max['grp']) #create list for all groups identified in large number

		ocr_df_pls4['grp_filt'] = ocr_df_pls4.apply (lambda row: self.remove_oth_grp(row,to_keep_grp),axis=1) #tries to keep group identified in list and alter other groups to 0

		return ocr_df_pls4,numeric_columns_grp

	#STEP6**########################################################################
	#################################################################################
	def _extract_Categ(self,sub_df,numeric_columns_grp,word_found=False,cat_type=False):
		"""
		Code written to extract the category and subcategory from the passed DF of single line
		use cat_type = True for if some match word like(JUMLAH and Total) is found.
			if cat_type == True then word_found should be provided as 'text_conv' of word JUMLAH found in list[in the end that will be index]

		"""
		#convert all text into list
		row_list = list(sub_df['text_conv'])
		#get index of words where numbers are present
		subdf_num_idx_list = list(sub_df[sub_df['grp_filt'].isin(numeric_columns_grp)].index)
		low_numeric = min(subdf_num_idx_list)
		if cat_type == False:
			if low_numeric>0: #means numeric is not first entry itself
				categ = ' '.join(row_list[:low_numeric]) #extract the text before first numeric instance found
				value = {sub_df.loc[i].grp_filt:row_list[i] for i in subdf_num_idx_list} #create a dict with group:numeric ##{1: 99346725553, 2: 77663382855}
			else:
				return (0,0)
		else:
			if low_numeric>0:
				category = [] #initiate the category list to have all strings inside for category line
				Flag = False #means any word will not be picked into category until Flag == True.[useful for cases like "ABC JUMLAH ASET"]
				for row_words in row_list[:low_numeric]: ##iterate just before numeric column present
					if row_words == word_found: #to extract the real category it should be present just after word 'JUMLAH' or 'TOTAL' which was found
						Flag = True                 #so set Flag to True when word jumlah found in list
					elif Flag == True:            #Only extract category information when Jumlah found and till numeric column present
						category.append(row_words)
				if len(category) >0:
					categ = ' '.join(category)
					value = {sub_df.loc[i].grp_filt:row_list[i] for i in subdf_num_idx_list} #dict with group:numeric ##{1: 99346725553, 2: 77663382855}
				else:
					print("Not able to find category: categ in text: {}".format(row_list))
					return (0,0)
		return (categ,value)

	def extract_labels_dict(self,df,numeric_columns_grp):
		'''
		Returns the dictionary with all the Subcategory labels nested under Category Labels.
		Ideally shoukd work with multiple numeric columns(2 or more) in row till text is in left and numbers on right[but test with 2 only].

		Logic:
			step1: find number of lines/block which contain any group value!=0(means identified as vertical column group of numbers)
			Step2: Iterate for all these lines in blocks with below 2 condition:
				a). If Word like 'JUMLAH' or 'TOTAL' not present in Row then that row belongs to Subcategory
				b). If above words contain in row then that is Category and all previous sub-category should fall inside this Category.
			STEP3: Return the final dictionary formed with S.no->>Category->>Subcategory->>[group:value,..] hierarchy

	 {1: {'LIABILITAS JANGKA PENDEK': {
	   'TOTAL': {1: '10568321162', 3: '9511146135'},
	   'UTANG BANK': {1: '669987120', 3: '641136000'},
	   'UTANG LAIN LAIN': {1: '911227865', 3: '871988388'},
	   'UTANG USAHA': {1: '8987106177', 3: '7998021747'}}},
	 2: {'LIABILITAS JANGKA PANJANG': {
	   'TOTAL': {1: '2243134509', 3: '2146540200'},
	   'UTANG BANK': {1: '856878264', 3: '819979200'},
	   'UTANG LAIN LAIN': {1: '1386256245', 3: '1326561000'}}},
	 3: {'LIABILITAS': {
	   'TOTAL': {1: '12811455671', 3: '11657686335'}}},
	 4: {'EKUITAS': {
	   'MODAL SAHAM': {1: '1000000000', 3: '1000000000'},
	   'SALDO LABA': {1: '88724865455', 3: '69922989697'},
	   'TOTAL': {1: '89724865455', 3: '70922989697'}}},
	 5: {'LIABILITAS DAN EKUITAS': {
	   'TOTAL': {1: '102536321126', 3: '82580676031'}}}}

df = ocr_df_pls4_grpfilt
		'''
		all_lines_with_numeric = sorted(list(set(df[df.grp_filt != 0].line_num))) #get all distinct lines where numeric entry present and are classified into groups
		print("ALL Lines with NUmeric:::::::::",all_lines_with_numeric)
		all_block_with_numeric = list(set(df[df.grp_filt != 0].block_num))

		#match = []
		dic = {}
		temp_dic = {}
		j=1
		for block in all_block_with_numeric:
			for line in all_lines_with_numeric:
				#block =1
				#line = 15 21

				sub_df = df[(df.block_num == block) & (df.line_num == line)].loc[:,['word_num','text_conv','numeric','grp_filt']]
				sub_df = sub_df.reset_index(drop=True) #reset the index of this sub group is important as will be using Index to find many logic below

				row_text_list = list(sub_df['text_conv']) #convert column text_conv content into list
				match = get_close_matches('JUMLAH', row_text_list, 1, 0.70) #find JUMLAH if match
				if not match: match = get_close_matches('TOTAL', row_text_list, 1, 0.70) #if JUMLAH not match then search for total

				if match: #means it's category one #Scenario 1 it's Category
					cat,cat_value = self._extract_Categ(sub_df,numeric_columns_grp,word_found=match[0],cat_type=True)
					if cat != 0: #means found some category
						temp_dic['TOTAL'] = cat_value #append into dictionary Total value of Category
						dic[j] = {cat:temp_dic} #append into main dictionary this Category found and Dictionary of Values of all Subcategories
						j += 1 #increment the counter for next value
						temp_dic = {} #emmpty the sub_category temp dictionary for next sub_categories.
					else:
						print("not able to properly find Category Name and Values in: {}".format(row_text_list))
						temp_dic = {}

				else: #means it's non category found and general subcategory with value
					subcat,subcat_value = self._extract_Categ(sub_df,numeric_columns_grp)
					if subcat != 0:
						temp_dic[subcat] = subcat_value
					else:
						print("not able to extract the Subcategory name and Value in line: {}".format(row_text_list))
		return dic


	#STEP7**################################################################################
	################################################################################
	def create_groupwise_categorised_dict(self,in_dict):
		"""
		Break the single Dictionary for multiple group values into different Group and their Categories found.
		eg. {ASSETS: {TOTAL:{1:100, 2:300}}} --to-->> {1:{ASSETS:{TOTAL:100}}, 2:{ASSETS:{TOTAL:300}}}
		{1: {1: {'LIABILITAS JANGKA PENDEK': {'TOTAL': '9511146135',
	    'UTANG BANK': '641136000',
	    'UTANG LAIN LAIN': '871988388',
	    'UTANG USAHA': '7998021747'}},
	  2: {'LIABILITAS JANGKA PANJANG': {'TOTAL': '2146540200',
	    'UTANG BANK': '819979200',
	    'UTANG LAIN LAIN': '1326561000'}},
	  3: {'LIABILITAS': {'TOTAL': '11657686335'}},
	  4: {'EKUITAS': {'MODAL SAHAM': '1000000000',
	    'SALDO LABA': '69922989697',
	    'TOTAL': '70922989697'}},
	  5: {'LIABILITAS DAN EKUITAS': {'TOTAL': '82580676031'}}},
	 3: {...}}
		""" #Previous some bug
		'''dd={}

		#Break the multi group dict into 2 different block
		for k,cat_dict in in_dict.items(): ## 1, {'LIABILITAS JANGKA PENDEK':{'TOTAL': '9
			main_dict ={}
			main_dict[k] = cat_dict
			print(main_dict,'\n')
			for cat,subcat_dict in cat_dict.items():  #'LIABILITAS JANGKA PENDEK' , {'TOTAL': {1: '10568321162', 3: '9511146135'}..
				#main_dict[k][cat] = subcat_dict
				for subcat,grp_dict in subcat_dict.items():  #'TOTAL' , {1: '10568321162', 3: '9511146135'}
					#main_dict[k][cat][subcat] = 0
					for grp,value in grp_dict.items(): # 1 , '10568321162'
						#print("creating for group: {}, values {}".format(grp,value))
						dd[grp] = main_dict
						dd[grp][k][cat][subcat] = value
		return dd
	  '''
		dd = {}
		for k,cat_dict in in_dict.items():
			for cat,subcat_dict in cat_dict.items():
				for subcat,grp_dict in subcat_dict.items():
					for grp,values in grp_dict.items():
						print(k,'+',cat,'+',subcat,'+',grp,'+',values)
						try: dd[grp]
						except: dd[grp] = {}
						try: dd[grp][k]
						except: dd[grp][k] = {}
						try:dd[grp][k][cat]
						except: dd[grp][k][cat] = {}
						try: dd[grp][k][cat][subcat]
						except:dd[grp][k][cat][subcat] = values
		return dd

	#Step8**############################################################################
	################################################################################
	def __find_blank_lst(self,lst):
		''' Return the length of non-empty list inside list'''
		tot_non_empty = 0
		for i in range(len(lst)):
			if not not lst[i]:
				tot_non_empty += 1
		return tot_non_empty

	def _map_found_category_with_known_categories(self,categories_value_dict,close_threshold = 0.70):
		dd = {'LIABILITIES_EQUITY':99,
			  'EQUITY':99,
			  'LIABILITIES_SHORT':99,
			  'LIABILITIES_LONG':99,
		    'LIABILITIES_CURRENT':99,
		    'LIABILITIES':99,
				'ASSETS':99,
				"ASSETS_CURRENT":99,
				"ASSETS_NONCURRENT":99}

		for k,v in categories_value_dict.items():
			for cat,val in v.items():
				cat_list = cat.split(" ")
				print("checking {} category".format(cat))

				#For All Assets type Current, Non Current and Total
				match_Asset = [get_close_matches(elem, ['ASET','TIDAK','LANCAR'], 1, close_threshold) for elem in cat_list]
				match_Asset_NNull = self.__find_blank_lst(match_Asset)

				if match_Asset_NNull == 3 and dd['ASSETS_NONCURRENT'] == 99:
					dd['ASSETS_NONCURRENT'] = k
					print("Belongs to Non Current Assets\n..........")
					continue
				elif match_Asset_NNull == 2 and dd['ASSETS_CURRENT'] == 99:
					dd['ASSETS_CURRENT'] = k
					print("Belongs to Current Assets\n..........")
					continue
				elif match_Asset_NNull == 1 and dd['ASSETS'] == 99:
					dd['ASSETS'] = k
					print("Belongs to Total Assets\n..........")
					continue

				#For Long term Liabilities
				match_LL = [get_close_matches(elem, ['KEWAJIBAN','LIABILITAS','PANJANG'], 1, close_threshold) for elem in cat_list]
				match_LL_NNull = self.__find_blank_lst(match_LL)
				if match_LL_NNull == 2 and dd['LIABILITIES_LONG'] == 99:
					dd['LIABILITIES_LONG'] = k
					print("Belongs to Long term Liability\n..........")
					continue

				#For Short term Liabilities
				match_LS = [get_close_matches(elem, ['KEWAJIBAN','LIABILITAS','PENDEK'], 1, close_threshold) for elem in cat_list]
				match_LS_NNull = self.__find_blank_lst(match_LS)
				if match_LS_NNull == 2 and dd['LIABILITIES_SHORT'] == 99:
					dd['LIABILITIES_SHORT'] = k
					print("Belongs to Short term Liability\n..........")
					continue

				#For Current Liabilities
				match_LC = [get_close_matches(elem, ['KEWAJIBAN','LIABILITAS','LANCAR'], 1, close_threshold) for elem in cat_list]
				match_LC_NNull = self.__find_blank_lst(match_LC)
				if match_LC_NNull == 2 and dd['LIABILITIES_CURRENT'] == 99:
					dd['LIABILITIES_CURRENT'] = k
					print("Belongs to Current Liability\n..........")
					continue

				#For Liabilities and Equity
				match_LE = [get_close_matches(elem, ['KEWAJIBAN','EKUITAS','LIABILITAS'], 1, close_threshold) for elem in cat_list]
				match_LE_NNull = self.__find_blank_lst(match_LE)
				if match_LE_NNull == 2 and dd['LIABILITIES_EQUITY'] == 99:
					dd['LIABILITIES_EQUITY'] = k
					print("Belongs to Total Liability and Equity\n..........")
					continue

				elif match_LE_NNull == 1 and len(match_LE) ==1: #condition for equity only
					#print("This word check happening: {}".format(match_LE[0][0]))
					if len(get_close_matches(match_LE[0][0], ['EKUITAS'], 1, close_threshold)) >0  and dd['EQUITY'] == 99 :
						dd['EQUITY'] = k
						print("Belongs to Equity\n..........")
						continue
					if len(get_close_matches(match_LE[0][0], ['LIABILITAS'], 1, close_threshold)) >0  and dd['LIABILITIES'] == 99 :
						dd['LIABILITIES'] = k
						print("belongs to Liability\n..........")
		return dd

	def map_found_categories_with_known_tags_multi(self,categorised_dict_text):
		"""
		It will use prebuild function "map_found_category_with_known_categories" but addition of multi group are added in below logic
		{1: {'ASSETS': 99,
	  'ASSETS_CURRENT': 99,
	  'ASSETS_NONCURRENT': 99,
	  'EQUITY': 4,
	  'LIABILITIES': 3,
	  'LIABILITIES_CURRENT': 99,
	  'LIABILITIES_EQUITY': 5,
	  'LIABILITIES_LONG': 2,
	  'LIABILITIES_SHORT': 1},
		3: {...}}
		"""
		dd = {}
		for k,cat_dict in categorised_dict_text.items():
			print("\nPeforming mapping for group: {}\n_______________________________".format(k))
			dd[k] =  self._map_found_category_with_known_categories(cat_dict)
		return dd

	#Step9**############################################################################
	##################################################################################
	def _final_dict_return(self,mapped_categorised_dict,categorised_dict_text):
		dd={}
		for category,idx in mapped_categorised_dict.items():
			if idx!=99:
				cat_blob = categorised_dict_text.get(idx,idx)
				for k,text in cat_blob.items():
					dd[category] = text
		return dd

	def final_dict_return_multi(self,mapped_categorised_dict,categorised_dict_text):
		"""
		Using prebuild method "final_dict_return", to return final dict with known tags and their values.
	 {1: {'EQUITY': {'MODAL SAHAM': '1000000000',
	   ' SALDO LABA': '88724865455',
	    'TOTAL': '89724865455'},
	   'LIABILITIES': {'TOTAL': '12811455671'},
	   'LIABILITIES_EQUITY': {'TOTAL': '102536321126'},
	   'LIABILITIES_LONG': {'TOTAL': '2243134509',
	    'UTANG BANK': '856878264',
	    'UTANG LAIN LAIN': '1386256245'},
	   'LIABILITIES_SHORT': {'TOTAL': '10568321162',
	    'UTANG BANK': '669987120',
	    'UTANG LAIN LAIN': '911227865',
	    'UTANG USAHA': '8987106177'}},
	 3: {'EQ...}}
		"""
		dd={}
		for k,found_tag in mapped_categorised_dict.items():
			dd[k] = self._final_dict_return(found_tag,categorised_dict_text[k])
		return dd

	#Step10**############################################################################
	##################################################################################
	def find_not_mapped_found_categories(self,multiple_cat_dict,categorised_dict_text,mapped_categorised_dict):
		''''find if something came into Categories but can't map to known tags.'''
		found_num_category = [k for k in multiple_cat_dict] #find all numbers in categories
		groups = [k for k in mapped_categorised_dict] #find total vertical groups in dict

		not_mapped_dd ={} #to return dict with all entry not able to match

		for grp in groups: #iterate for all groups identified
			mapped_num_category = [v for k,v in mapped_categorised_dict[grp].items() if v!=99]	 #extract all cat number present in mapped dict.
			diff = list(set(found_num_category) - set(mapped_num_category)) #find if any difference present between found cat and mapped cat
			if len(diff) >0: #if exist difference
				print("Some categories not able to map Please Check, {}".format(diff))
				for cat in diff:
					try: not_mapped_dd[grp]
					except: not_mapped_dd[grp] ={}
					not_mapped_dd[grp][cat] = categorised_dict_text[grp][cat]

		return not_mapped_dd

	# Accumulating all steps into single function ################################
	##############################################################################

	def cur_time(self,arg1):
		return datetime.datetime.now().strftime(arg1)

	def call_extract_categories(self,output_img):
		'''This function will call all the steps linearly and return just json with category found and not mapped		'''
		logname = 'logs/'+output_img.split('.')[0]+'_ocr_'+self.cur_time("%d%m%Y")+'.log'
		with open(logname,'a+') as f:
			f.write("\n=============== {} ===================\n".format(self.cur_time("%H:%M:%S")))
			#f.write("Request Received at {}\n".format(self.cur_time("%H:%M:%S")))
			#step2
			ocr_raw = self.perform_ocr_on_image(output_img)
			ocr_df = self.create_dataframe_from_ocr_raw(ocr_raw)
			f.write(">>>> step2 DONE [tesseract ran] at {}\n".format(self.cur_time("%H:%M:%S")))

			#step3
			exclude = set(string.punctuation)
			ocr_df_pls4 = self.create_derived_cols(ocr_df,exclude)
			f.write(">>>> step3 DONE [4 derived cols created] at {}\n".format(self.cur_time("%H:%M:%S")))

			#step4
			ocr_df_pls4['grp'] = ocr_df_pls4.apply(lambda row: self.p.calc_vertical_group(row),axis=1)
			ocr_df_pls4.to_csv(f, index=False) #write the dataframe into log file
			f.write(">>>> step4 DONE [rgouping based on vertical cols] at {}\n".format(self.cur_time("%H:%M:%S")))

			#step5
			ocr_df_pls4_grpfilt,numeric_columns_grp  = self.remove_unwanted_groups_new(ocr_df_pls4)
			ocr_df_pls4_grpfilt = ocr_df_pls4_grpfilt[~ocr_df_pls4_grpfilt.text_conv.isin(['-',':','.','@','—','|'])] #remove unwanted records from dataframe where only
			f.write("      Total groups Identified: {}\n".format(len(numeric_columns_grp)))
			f.write(">>>> step5 DONE [filter out non numerical groups] at {}\n".format(self.cur_time("%H:%M:%S")))
			del(ocr_raw,ocr_df,ocr_df_pls4)

			#if len(numeric_columns_grp) == 0:
			#	f.write("     "+str(ocr_df_pls4_grpfilt)+"\n")
			#	f.write(">>>> ERROR <<<< - No numeric group identified in the Dataframe read\n")
			#	raise NameError("No numeric group has been found in block")

			#step6
			multiple_cat_dict = self.extract_labels_dict(ocr_df_pls4_grpfilt,numeric_columns_grp)
			f.write(">>>> step6 DONE [find labels of filter groups] at {}\n".format(self.cur_time("%H:%M:%S")))
			print(multiple_cat_dict)

			#step7
			categorised_dict_text = self.create_groupwise_categorised_dict(multiple_cat_dict) #tried to standardise single and double group format
			f.write(">>>> step7 DONE [split for multiple groups if found] at {}\n".format(self.cur_time("%H:%M:%S")))

			##step8
			mapped_categorised_dict = self.map_found_categories_with_known_tags_multi(categorised_dict_text)
			f.write(">>>> step8 DONE [map with known Categories] at {}\n".format(self.cur_time("%H:%M:%S")))

			##step9
			fin_result = self.final_dict_return_multi(mapped_categorised_dict,categorised_dict_text)
			#pickle.dump(fin_result, f)
			f.write("     "+str(fin_result)+"\n")
			f.write(">>>> step9 DONE [change the structure of dict return] at {}\n".format(self.cur_time("%H:%M:%S")))

			##step10
			not_mapped_found_categories = self.find_not_mapped_found_categories(multiple_cat_dict,categorised_dict_text,mapped_categorised_dict)
			#pickle.dump(not_mapped_found_categories, f)
			f.write("     "+str(not_mapped_found_categories)+"\n")

			f.write(">>>> step10 DONE [Find any non mapped tags] at {}\n".format(self.cur_time("%H:%M:%S")))
#		except NameError:
#			print("No numeric group has been found in block this block")
#			fin_result = {}
#			not_mapped_found_categories = {}

#		except OSError as e:
#			if e.errno == 2:
#	        # suppress "No such file or directory" error
#				print("File not found or can't open")
#			fin_result = {}
#			not_mapped_found_categories = {}



		return (fin_result,not_mapped_found_categories,logname)

if __name__ == '__main__':


	#input_pdf = r"C:\financial_scan\BalanceSheet.pdf"
	output_img = "fin1_5.jpg"
	#output_img = "BalanceSheet_1.jpg"
	ocr = convert_ocr() #inititate the class  first

	##Either call belo function for all pipeline inititate
	(fin_dict,not_map,log_filename) = ocr.call_extract_categories(output_img)

	##Or call below steps in detail for every errors
	##Step1:: Convert single page of pdf to image
	#output_img = convert_pdf_2_image(input_pdf,pages = '0')
	###When using R service to convert
	#output_img = convert_pdf2Image_in_R(input_pdf,pages='1') #in R index starts with 1 instead of 0
	#output_img = output_img[0]

	#start1 = 0
	##Step2:: Perform OCR on image and format into dataframe
	start2 = time.time()
	ocr_raw = ocr.perform_ocr_on_image(output_img)
	ocr_df = ocr.create_dataframe_from_ocr_raw(ocr_raw)

	##Step3:: create 4 additional derived columns for future processing viz. Right,Center,Numeric and Text_conv
	start3 = time.time()
	exclude = set(string.punctuation)
	ocr_df_pls4 = ocr.create_derived_cols(ocr_df,exclude)

	##Step4:: Assign groups to each text based on x-axis
	start4 = time.time()
	p = closeness()
	ocr_df_pls4['grp'] = ocr_df_pls4.apply(lambda row: p.calc_vertical_group(row),axis=1)

	##step5::find the groups using elbow method
	start5 = time.time()
	ocr_df_pls4_grpfilt,numeric_columns_grp  = ocr.remove_unwanted_groups_new(ocr_df_pls4)
	ocr_df_pls4_grpfilt = ocr_df_pls4_grpfilt[~ocr_df_pls4_grpfilt.text_conv.isin(['-',':','.','@','—','|'])] #remove unwanted records from dataframe where only these are present

	#total_numeric_columns = len(numeric_columns_grp)

	##step6::Returns the dictionary with all the Subcategory labels nested under Category Labels.
	start6 = time.time()
	multiple_cat_dict = ocr.extract_labels_dict(ocr_df_pls4_grpfilt,numeric_columns_grp)

	##step7::Break the single Dictionary for multiple group values into different Group and their Categories found.
	start7 = time.time()
	categorised_dict_text = ocr.create_groupwise_categorised_dict(multiple_cat_dict) #tried to standardise single and double group format

	##step8::It will try to map the known category/Tags with found category
	start8 = time.time()
	mapped_categorised_dict = ocr.map_found_categories_with_known_tags_multi(categorised_dict_text)

	##step9::Retrun the Final dict with known tags and their found subcategories
	start9 = time.time()
	fin_result = ocr.final_dict_return_multi(mapped_categorised_dict,categorised_dict_text)

	##step10::Find the not mapped categories which found
	start10 = time.time()
	not_mapped_found_categories = ocr.find_not_mapped_found_categories(multiple_cat_dict,categorised_dict_text,mapped_categorised_dict)
	start11 = time.time() #actually end time


	print(fin_result)
	print(not_mapped_found_categories)
	for i in range(3,12):
		print("Time taken in step{} :{}sec\n".format(i-1,round(globals()["start"+str(i)]-globals()["start"+str(i-1)],2)))


