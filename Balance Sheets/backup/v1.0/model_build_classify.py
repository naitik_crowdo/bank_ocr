# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 18:30:08 2018

@author: CWDSG05
"""

#from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm
#from sklearn import decomposition, ensemble
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.naive_bayes import MultinomialNB
import os.path
import pickle

class classify(object):
	def __init__(self):
			self.labels_excel_match_ASSETS_CURRENT = {
																		1:"Cash and Cash Equivalent", #KAS DAN SETARA KAS, 2 in file
																		2:"Accounts Receivable", #PIUTANG AFILIASI,4
																															#PIUTANG LAIN LAIN,5
																															#PIUTANG USAHA, 6
																		3:"Payment in Advance", #BIAYA DIBAYAR DIMUKA,1	 <<-- 'Prepaid Expenses'
																		4:"Inventories, net", #PERSEDIAAN,3  <<-- Inventories
																		5:"Short-term investments",
																		6:"Stock and work in progress",
																		7:"Other Current Assets",
																		8:"Total"}

			self.labels_excel_match_ASSETS_NONCURRENT = {
																		1:"Property, plant, and equipment",
																		2:"Less:  accumulated depreciation",
																		3:"Investment Property",  #TRANSLATED:: PROPERTI INVESTASI
																		4:"Goodwill, net", #ASET TETAP NILAI BUKU
																		5:"Other intangibles, net",
																		6:"Other deferred taxes",
																		7:"Other non-current assets",
																		8:"Total"}

			self.labels_excel_match_LIABILITIES_CURRENT = {
																		1:"Accounts payable", #from current ## HUTANG USAHA
																		2:"Short-term debt", #HUTANG BANK (Bank loan)
																		3:"Current portion of long-term debt",
																		4:"Accrued liabilities", #from current ## BIAYA YANG MASIH HARUS DIBAYAR (Accrued cost)
																		5:"Income taxes payable",  #from current ## HUTANG PAJAK (Tax payable)
																		6:"Other Payables", #from current ## HUTANG LAIN-LAIN(Other Debts)
																		7:"Total"}

			self.labels_excel_match_LIABILITIES_NONCURRENT = {
																		1:"Bank Loan", # HUTANG BANK #UTANG BANK
																		2:"other Long Term Loan", # HUTANG PEMEGANG SAHAM(Shareholder's debt) #UTANG LAIN LAIN(Other payable)
																		3:"Total"}

			self.labels_excel_match_LIABILITIES = {
																		1:"Deffered Income Taxes Payable", #translated-- HUTANG PAJAK PENGHASILAN DITANGGUHKAN
																		2:'Other deferred Liabilities', #translated KEWAJIBAN DITANGGUHKAN LAIN LAIN
																		3:"Other Liabilities", # KEWAJIBAN LAIN LAIN(Other Liabilities)
																		4:"Total"
																			}

			self.labels_excel_match_EQUITY = {
																		1:"Preferred stock", #MODAL SAHAM
																		2:"Common stock, par value plus additional paid-in capital",
																		3:"Retained earnings (accumulated deficit)", #SALDO LABA
																		4:"Other stockholders' equity",
																		5:"Total"}


	def train_naive_bayes(self,cat_type,new=False):
		'''
		cat_type can be: ASSETS_CURRENT, ASSETS_NONCURRENT, LIABILITIES_SHORT,LIABILITIES_CURRENT, LIABILITIES_LONG, EQUITY
		'''
		if new == False:
			file_path = 'train_data/'+cat_type+'_train.csv'
			if os.path.isfile(file_path):
				data = pd.read_csv(file_path)
				print("READ the file for training: {}".format(file_path))
				y = data['label']
				x = data['text']

				###Start Preprocessing
				# characters level tf-idf inititator
				tfidf_vect_ngram_chars = TfidfVectorizer(analyzer='char_wb', token_pattern=r'\w{1,}', ngram_range=(2,3), max_features=5000)
				tfidf_vect_ngram_chars.fit(x) #train on all x for ngram

				#Save vectorizer.vocabulary
				vocabulary_filename = 'features_extracted/'+cat_type+"_feature.pkl"
				print("SAVE the file for ngram Vocabulary: {}".format(vocabulary_filename))
				pickle.dump(tfidf_vect_ngram_chars.vocabulary_,open(vocabulary_filename,"wb"))
				#del(tfidf_vect_ngram_chars)

				#Transform to ngram
				xtrain_tfidf_ngram_chars =  tfidf_vect_ngram_chars.transform(x)
				##tfidf_vect_ngram_chars.get_feature_names()  #to see ngrams

				#inititate the model which we want to train
				classifier = MultinomialNB()
				#classifier = GaussianNB()
				#classifier = BernoulliNB()
				#classifier = xgboost.XGBClassifier()

				# fit the training dataset on the classifier
				classifier.fit(xtrain_tfidf_ngram_chars, y)

		    #save the model to disk
				model_filename = 'models/'+cat_type+'_model.sav'
				pickle.dump(classifier, open(model_filename, 'wb'))
				print("SAVE the Trained Model: {}".format(model_filename))
				return True
			else:
				print("Entered wrong category, No file found with category = {}".format(cat_type))
				return False
		else:
			print("set new to blank for default")
			return False

	def predict_subcat(self,cat_type,raw_x):
		raw_x = [x.upper() for x in raw_x]
		valid_categ = ['ASSETS_CURRENT', 'ASSETS_NONCURRENT', 'LIABILITIES_CURRENT','LIABILITIES_NONCURRENT','EQUITY']
		if cat_type in valid_categ:
			##If block only for all correct category type::
			model_file_path = "models/"+cat_type+"_model.sav"
			if os.path.isfile(model_file_path):

				#globs = globals()
				#locs = locals()

				#load the model from disk
				classifier = pickle.load(open(model_file_path, 'rb'))

				##Load the vectorizer vocabulary
				vocabulary_filename = 'features_extracted/'+cat_type+"_feature.pkl"
				if os.path.isfile(vocabulary_filename):
					tfidf_vect_ngram_chars = CountVectorizer(analyzer='char_wb', ngram_range=(2,3), max_features=5000,vocabulary=pickle.load(open(vocabulary_filename, "rb")))

					#perform ngram on predict type
					xvalid_tfidf_ngram_chars =  tfidf_vect_ngram_chars.transform(raw_x)

					#predict the labels on validation dataset
					predictions = classifier.predict(xvalid_tfidf_ngram_chars) #predict the  subcategory from preprocessed input received
					#print("self.labels_excel_match_"+cat_type+"[j]")
					#print("found category",[(raw_x[i],eval("self.labels_excel_match_"+cat_type+"[j]",globals(),locals())) for i,j in enumerate(predictions)])

					to_send = list()
					label = "self.labels_excel_match_"+cat_type
					for i,j in enumerate(predictions):
						to_send.append(eval(label+"[j]")) #append the list to be return using eval
						#print("found sub-category for:",raw_x[i],"--> ",eval(label+"[j]")) #print the raw and processed values
					return to_send

					#return [eval("self.labels_excel_match_"+cat_type+"[j]",globs,locs) for i,j in enumerate(predictions)]
				else:
					print("Vocabulary not found with Name: {}".format(vocabulary_filename))
			else:
					print("Model not found with Name: {}".format(model_file_path))
		else:
			if cat_type in ["ASSETS","LIABILITIES","LIABILITIES_EQUITY","EQUITY"] and len(raw_x)==1 and raw_x[0]=="TOTAL":
				return ['TOTAL']
			elif cat_type == "ASSETS" and len(raw_x)==2:
				if raw_x[0]=="ASET LAIN LAIN" and raw_x[1]== 'TOTAL':
					return ['Total','Other Assets']
				elif raw_x[1]=="ASET LAIN LAIN" and raw_x[0]== 'TOTAL':
					return ["Other Assets",'Total']
		print("Category input:{} not in valid categories {}".format(cat_type,valid_categ))
		return 1

if __name__ == '__main__':

	globs = globals()
	locs = locals()


	x_test = ['KAS SETARA','PIUTANG LAIN']
	y_test = ['Cash and Cash Equivalent', 'Accounts Receivable']

	p = classify()

	p.train_naive_bayes("ASSETS_CURRENT")
	y_predict =p.predict_subcat("ASSETS_CURRENT",x_test)


	p.predict_subcat("ASSETS_CURRENT",x_test)

	dd_test = ["as","ddd"]
	fi = "dd"
	print(eval(fi+"_test"))

	predictions = [1,2,3,4]
	labels_excel_match_ASSETS_CURRENT = ['a','b','c','d']
	cat_type = "ASSETS_CURRENT"
	eval(["labels_excel_match_"+cat_type+"[j]" for i,j in enumerate(predictions)])

	[eval("labels_excel_match_"+cat_type+"[i]") for i,j in enumerate(predictions)]
