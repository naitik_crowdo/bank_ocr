# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 10:23:09 2018

@author: CWDSG05
"""

import os
os.chdir(r"C:\financial_scan\scripts\working_all_classes_v1")

from script_modules.extract_categories_v1 import convert_ocr
from script_modules.model_build_classify import classify
import datetime

def cur_time(arg1):
	return datetime.datetime.now().strftime(arg1)

def predict_template_subcateg(fin_result,f):
	##Below code will go into the dict and for each subcategories under categories of ASSETS,ASSETS_CURRENT,ASSETS_NONCURRENT,LIABILITIES,LIABILITIES_CURRENT,
	# LIABILITIES_NONCURRENT, EQUITY, EQUITY_AND_LIABILITIES
	## It will predict for these subcat into corresponding excel_template matching texts.
	## Once return the result it will replace the keys in dictionary with new found subcategories

	##############################################
	# Change the LIABILITIES_SHORT to CURRENT_LIABILITIES
	# and LIABILITIES_LONG to NONCURRENT_LIABILITIES
	f.write("\t\t Replacing LIABILITIES_SHORT or LIABILITIES_LONG\n")
	for groups,details in fin_result.items():
		for categ in details:
			if categ =='LIABILITIES_SHORT':
				fin_result[groups]['LIABILITIES_CURRENT'] = fin_result[groups].pop(categ)
			elif categ =='LIABILITIES_LONG':
				fin_result[groups]['LIABILITIES_NONCURRENT'] = fin_result[groups].pop(categ)
	##############################################
	f.write("\t\t Iterating to predict in dictionary return\n")
	for groups,details in fin_result.items():
		print("\t\t{}".format(groups))
		f.write("\n\t\t GROUP: {}\n".format(groups))
		for categ,subcat_det in details.items():
			print("\t\t\t{}".format(categ))
			f.write("\t\t\t {}\n".format(categ))
			x_test = [x for x in subcat_det]
			print("\t\t\t\t original tags: {}".format(x_test))
			f.write("\t\t\t\t Original Tags: {}\n".format(x_test))
			y_predict =model.predict_subcat(categ,x_test)  #Call for predict here using prebuild models
			if y_predict == 1:
				print(">>>>>>> ERROR <<<<<<<< in Prediction")
				f.write("\t\t\t\t >>>>> ERROR <<<<<< in prediction, status 1\n")
			else:
				print("\t\t\t\tmapped tags: {}".format(y_predict))
				print("-----------------------")
				f.write("\t\t\t\t mapped Tags: {}\n".format(y_predict))
				f.write("\t\t\t\t --------------------------\n")

				#fin_result[groups][x for x in y_predict] = fin_result[groups].pop([str(y) for y in x_test])
				for i,j in enumerate(x_test):
					fin_result[groups][categ][y_predict[i]] = fin_result[groups][categ].pop(j)

				#fin_result[groups][x for x in y_predict] = fin_result[groups].pop([str(y) for y in x_test])
	return fin_result

def train_model(categ):
	if model.train_naive_bayes(categ):
		print("\t\t\t Model for {} trained successfull\n".format(categ))
	else:
		print("\t\t\t Training model >>>>>FAIL<<<< for {}, Check the Training file\n".format(categ))


if __name__ == '__main__':
	ocr = convert_ocr()
	model = classify()

	output_img = "fin1_5.jpg"

	(fin_result,not_matched,logname) = ocr.call_extract_categories(output_img)

	with open(logname,'a+') as f:
		f.write("\n========== OCR extraction Finish: {} ===================\n".format(cur_time("%H:%M:%S")))
		fin_result_mapped = predict_template_subcateg(fin_result,f)
		f.write("\n========== Prediction  call Finish: {} ===================\n".format(cur_time("%H:%M:%S")))

	all_categ = ['ASSETS_CURRENT','ASSETS_NONCURRENT','LIABILITIES_CURRENT','LIABILITIES_NONCURRENT','LIABILITIES','EQUITY']
	for categ in all_categ:
		train_model(categ)

	## Test for specific subcategories
	categ = 'LIABILITIES_CURRENT'
	x_test = list(fin_result[1][categ].keys())

	for groups,details in fin_result.items():
		for categ in details:
			if categ =='LIABILITIES_SHORT':
				fin_result[groups]['LIABILITIES_CURRENT'] = fin_result[groups].pop(categ)
			elif categ =='LIABILITIES_LONG':
				fin_result[groups]['LIABILITIES_NONCURRENT'] = fin_result[groups].pop(categ)


	y_predict = model.predict_subcat(categ,x_test)  #Call for predict here using prebuild models


	######################################################
	## Going deep into the code inside
	raw_x = x_test
	cat_type = categ

