# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 16:47:32 2018

@author: CWDSG05
"""

from flask import Flask, jsonify, abort, make_response, request,send_file
import json,os
from numpy import int0
import datetime

os.chdir(r"C:\financial_scan\scripts\working_all_classes_v1")

from script_modules.extract_categories_v1 import convert_ocr
from script_modules.model_build_classify import classify

def cur_time(arg1):
	return datetime.datetime.now().strftime(arg1)

ocr = convert_ocr()
model = classify()

NOT_FOUND = 'Not found'
BAD_REQUEST = 'Bad request'
NO_PDF = 'PDF/Image not found at path'
NO_EXEC_OCR = 'Error while extracting OCR category tags'
NO_EXEC_PREDICT = 'Error while Predicting OCR sub-category tags'

app = Flask(__name__)

@app.errorhandler(404)
def not_found(error):
	return make_response(jsonify({'error': NOT_FOUND}), 404)

@app.errorhandler(400)
def bad_request(error):
	return make_response(jsonify({'error': BAD_REQUEST}), 400)

@app.errorhandler(408)
def pdf_not_found(error):
	return make_response(jsonify({'error': NO_PDF}), 408)

@app.errorhandler(412)
def failed_execution_part1(error):
	return make_response(jsonify({'error': NO_EXEC_OCR}), 412)

@app.errorhandler(416)
def failed_execution_part2(error):
	return make_response(jsonify({'error': NO_EXEC_PREDICT}), 412)

@app.route('/')
def hello_world():
	return jsonify({'Balance_sheet OCR': 'server is active'}), 200

######################################### DEFINE THE FUNCTIONS WE GONNA USE FOR API USAGE ######################################
################################################################################################################################

def predict_template_subcateg(fin_result,f,model):
	'''
Below code will go into the dict and for each subcategories under categories of
	ASSETS,   ASSETS_CURRENT,  ASSETS_NONCURRENT,  LIABILITIES,  LIABILITIES_CURRENT,
	LIABILITIES_NONCURRENT,   EQUITY,  EQUITY_AND_LIABILITIES

	It will predict for these subcat into corresponding excel_template matching texts.
	Once return the result it will replace the keys in dictionary with new found subcategories
'''
	##############################################
	# Change the LIABILITIES_SHORT to CURRENT_LIABILITIES
	# and LIABILITIES_LONG to NONCURRENT_LIABILITIES
	f.write("\t\t Replacing LIABILITIES_SHORT or LIABILITIES_LONG\n")
	for groups,details in fin_result.items():
		for categ in details:
			if categ =='LIABILITIES_SHORT':
				fin_result[groups]['LIABILITIES_CURRENT'] = fin_result[groups].pop(categ)
			elif categ =='LIABILITIES_LONG':
				fin_result[groups]['LIABILITIES_NONCURRENT'] = fin_result[groups].pop(categ)
	##############################################
	f.write("\t\t Iterating to predict in dictionary return\n")
	for groups,details in fin_result.items():
		print("\t\t{}".format(groups))
		f.write("\n\t\t GROUP: {}\n".format(groups))
		for categ,subcat_det in details.items():
			print("\t\t\t{}".format(categ))
			f.write("\t\t\t {}\n".format(categ))
			x_test = [x for x in subcat_det]
			print("\t\t\t\t original tags: {}".format(x_test))
			f.write("\t\t\t\t Original Tags: {}\n".format(x_test))
			y_predict =model.predict_subcat(categ,x_test)  #Call for predict here using prebuild models
			if y_predict == 1:
				print(">>>>>>> ERROR <<<<<<<< in Prediction")
				f.write("\t\t\t\t >>>>> ERROR <<<<<< in prediction, status 1\n")
			else:
				print("\t\t\t\tmapped tags: {}".format(y_predict))
				print("-----------------------")
				f.write("\t\t\t\t mapped Tags: {}\n".format(y_predict))
				f.write("\t\t\t\t --------------------------\n")

				#fin_result[groups][x for x in y_predict] = fin_result[groups].pop([str(y) for y in x_test])
				for i,j in enumerate(x_test):
					fin_result[groups][categ][y_predict[i]] = fin_result[groups][categ].pop(j)

				#fin_result[groups][x for x in y_predict] = fin_result[groups].pop([str(y) for y in x_test])
	return fin_result


################################################################################################################################
################################################################################################################################

@app.route('/api/v1.0/bal_sheet/extract_img', methods=['POST','GET'])
def receive_sheet_image_name_and_return_extracted_categ_unmatchedTags():

	with app.app_context():
		if not request.json or 'file_path' not in request.json:
			print("Bad request received::")
			print(request.data)
			abort(400)

		print("request received :{}\n".format(request.json))
		output_img = request.json['file_path'] 	#extract image path from dictionary

		if not os.path.isfile(output_img):
			print("File is not present")
			abort(408)
		try:
			################## Calling Part1, for OCR the image ######################
			(fin_result,not_matched,logname) = ocr.call_extract_categories(output_img)
			print("Part1 completed")
		except:
			print("Part1 Failed")
			abort(412)

		################Calling part 2 for predict of found subcategories inside categories
		with open(logname,'a+') as f:
			f.write("\n========== OCR extraction Finish: {} ===================\n".format(cur_time("%H:%M:%S")))
			try:
				fin_result_mapped = predict_template_subcateg(fin_result,f,model)
				f.write("\n========== Prediction  call Finish: {} ===================\n".format(cur_time("%H:%M:%S")))
			except:
				f.write("\n========== Prediction  call >>>>FAILED<<<< : {} ===================\n".format(cur_time("%H:%M:%S")))
				print("Part2 Failed")
				abort(416)

		return jsonify({'Extracted_OCR': str(fin_result_mapped), 'NOT_MAPPED':str(not_matched)}), 200
		#return jsonify({'Extracted_OCR': fin_result_mapped, 'NOT_MAPPED':not_matched}), 200

###################### 2nd Endpoint for saving new label
########################################################
@app.route('/api/v1.0/bal_sheet/send_train', methods=['POST','GET'])
def receive_new_train_data_with_subcategory_and_save_to_file():
	'''
	Received json should be in below format::

		{'to_train':{'ASSETS_CURRENT':{'BIAYA DIBAYAR DIMUKA':'Payment in Advance'},
								   'ASSETS_NONCURRENT':{'ASET TETAP NILAI BUKU':'Goodwill, net'}
							   }
			}

	{to_train: { category : {'text in bahasa': 'already known subcategory'}
						}
	}
	'''

	if not request.json or 'to_train' not in request.json:
		print("Bad request received::")
		print(request.data)
		abort(400)
	print("request received :{}\n".format(request.json))
	retraining_text = request.json['to_train'] 	#extract training dict from big dictionary
	#print("retraining_text ::",retraining_text)
	try:
		for categ,subcat_dict in retraining_text.items():
			categ = categ.upper()
			filename = "train_data/"+categ+".newtrain"
			#print("New file name will be:",filename)
			if not os.path.isfile(filename): #means file is not present before
				with open(filename,'a+') as file:
					file.write("text@label\n")
					for bahasa_ocr,to_map_subcat in subcat_dict.items():
						bahasa_ocr = bahasa_ocr.upper()
						to_map_subcat = to_map_subcat.upper()
						mapped_number_frm_to_map = model.extract_key_from_value_self(categ,to_map_subcat)
						if mapped_number_frm_to_map != 99:
							file.write("{}@{}\n".format(bahasa_ocr,mapped_number_frm_to_map)) ##might need to get here mapping number for to_map string from model_build_classify.py
						else:
							text = "Not found match for: "+to_map_subcat+" under: "+categ
							return jsonify({'error': text}), 420
					print("\tSent for training: File:{},\n\t\t original text:{},\t Mapped subCategory:{}".format(filename,bahasa_ocr,mapped_number_frm_to_map))
			else:
				with open(filename,'a+') as file:
					for bahasa_ocr,to_map_subcat in subcat_dict.items():
						bahasa_ocr,to_map_subcat = bahasa_ocr.upper(), to_map_subcat.upper()
						#to_map_subcat = to_map_subcat.upper()
						mapped_number_frm_to_map = model.extract_key_from_value_self(categ,to_map_subcat)
						if mapped_number_frm_to_map != 99:
							file.write("{}@{}\n".format(bahasa_ocr,mapped_number_frm_to_map)) ##might need to get here mapping number for to_map string from model_build_classify.py
						else:
							text = "Not found match for: "+to_map_subcat+" under: "+categ
							return jsonify({'error': text}), 420
					print("\tSent for training: File:{}, \n\t\toriginal text:{},\t Mapped subCategory:{}".format(filename,bahasa_ocr,mapped_number_frm_to_map))
		return jsonify({'Status': 'SAVED'}), 200
	except:
		return make_response(jsonify({'error': 'Error while Saving'}), 420)



######################@nd Endpoint for training new model based on training data

## Function used in the endpoint
def backup_files_nd_train(categ,log):
	filename_new = "train_data/"+categ+".newtrain" #new training file which created from online submission
	filename_orig = "train_data/"+categ+".train" #existing file which is used for training model
	filename_orig_bkp = "train_data/bkp/"+categ+'_'+cur_time("%d%m_%y")+".train"
	filename_new_bkp = "train_data/bkp/"+categ+'_'+cur_time("%d%m_%y")+".newtrain"

	if os.path.isfile(filename_new): #only if new file is presen twith train data inside
		# Unix
		os.popen('cp '+filename_orig+' '+filename_orig_bkp) #first make backup of original train data
		log.write("\t\t\tCreated backup of file '{}' to '{}'".format(filename_orig,filename_orig_bkp))
		#### below is the code block to copy content from one file to another
		with open(filename_new) as f: ## open the file from where we have to copy the data (new file)
			with open(filename_orig, "a+") as f1: #open the file where we have to copy the new Data(original train file)
				f1.write("\n")
				for line in f: ## read each line in new data file
					if "text@label" not in line: #remove the headings to be written
						f1.write(line)  #just write each line

		os.popen('mv '+ filename_new +' '+filename_new_bkp) #move new training data file to backup folder

		train_status = model.train_naive_bayes(categ)
		if train_status:
			print("\t\t Trained successfully for {}".format(categ))
			log.write("\n\t\t Trained successfully for {}".format(categ))
		else:
			print("\t\t Failed training for {}".format(categ))
			log.write("\n\t\t Failed training for {}".format(categ))
			return False
	else:
		print("\t\tNo New file present to train for categ: {} ".format(categ))
		log.write("\t\tNo New file present to train for categ: {} ".format(categ))
	#bydefault return True
	return True


@app.route('/api/v1.0/bal_sheet/train_models', methods=['POST','GET'])
def train_new_models_with_merging_new_train_data():
	'''
	This endpoint accept json in format :: {'train_categ':['EQUITY','LIQUIDITY']}  or {'train_categ':['ALL']}
	where Individual categories inside list will be seen for any new files for train and if present retrain the model and features extract for them.

	If mention ALL, it will do above thing for all the possible categories which are::
	['ASSETS_CURRENT',  'ASSETS_NONCURRENT',
 'LIABILITIES_CURRENT',  'LIABILITIES_NONCURRENT',
 'LIABILITIES',  'EQUITY']

	'''
	#### Throwing error part on Bad Request
	if not request.json or 'train_categ' not in request.json:
		print("Bad request received::")
		print(request.data)
		abort(400)

	logname = 'fin_logs/train_model_'+cur_time("%d%m%Y")+'.log'

	with open(logname,'a+') as f:
		f.write("\n=============== {} ===================\n".format(cur_time("%H:%M:%S")))

		#### Extracting content from parameters
		print("request received :{}\n".format(request.json))
		f.write("request received :{}\n".format(request.json))
		training_cat_text = request.json['train_categ'] 	#extract training dict from big dictionary

		#### Main processing block
		if type(training_cat_text) is list:  ## Checking the type of parameters send is in list type
			for categ in training_cat_text: ## Iterate for all the values in list received
				categ = categ.upper() ## Cast to uppercase for better comparison


				if categ == "ALL":  ## Special Case when receive ALL means train all the models serially
					print('retrain for all categ available')
					f.write('\tRetraining for ALL')
					for categ in model.valid_categ:
						status = backup_files_nd_train(categ,f)
						if status == False:
							return jsonify({'error': 'This category training failed:: '+categ}), 435
					return jsonify({'success':'Retrained all Categories'}),200


				#### If other than ALL received then specific models need to train
				else:
					if categ in model.valid_categ: ## Check here if category received are actually all in valid categories of models
						status = backup_files_nd_train(categ,f)
						if status == False:
							return jsonify({'error': 'This category training failed:: '+categ}), 435
					else:
						f.write("\tReceived Category is not valid: ",categ)
						return jsonify({'error': 'This category is not Valid:: '+categ}), 430

			## default it will return success
			return jsonify({'success': 'Update model Successfull'}), 200
		else:
			return jsonify({'error': 'list of dict format expected {train_categ:["EQUITY"]}'}), 425

############Run the app in the last
app.run(host='0.0.0.0',port=5253,debug=True)


#if __name__ == '__main__':


	#output_img = "fin1_5.jpg"

#	app.run(host='0.0.0.0',port=5253,debug=True)

model.extract_key_from_value_self('EQUITY',"PREFERRED STOCKq")