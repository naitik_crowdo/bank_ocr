# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 18:30:08 2018

@author: CWDSG05
"""

#from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm
#from sklearn import decomposition, ensemble
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.naive_bayes import MultinomialNB
import os.path
import pickle

class classify(object):
	def __init__(self):

			self.valid_categ = ['ASSETS_CURRENT', 'ASSETS_NONCURRENT', 'LIABILITIES_CURRENT','LIABILITIES_NONCURRENT','LIABILITIES','EQUITY']

			self.labels_excel_match_ASSETS_CURRENT = {
																		1:"CASH AND CASH EQUIVALENT", #KAS DAN SETARA KAS, 2 IN FILE
																		2:"ACCOUNTS RECEIVABLE", #PIUTANG AFILIASI,4
																															#PIUTANG LAIN LAIN,5
																															#PIUTANG USAHA, 6
																		3:"PAYMENT IN ADVANCE", #BIAYA DIBAYAR DIMUKA,1	 <<-- 'PREPAID EXPENSES'
																		4:"INVENTORIES/NET", #PERSEDIAAN,3  <<-- INVENTORIES
																		5:"SHORT-TERM INVESTMENTS",
																		6:"STOCK AND WORK IN PROGRESS",
																		7:"OTHER CURRENT ASSETS",
																		8:"TOTAL"}

			self.labels_excel_match_ASSETS_NONCURRENT = {
																		1:"PROPERTY, PLANT, AND EQUIPMENT",
																		2:"LESS:  ACCUMULATED DEPRECIATION",
																		3:"INVESTMENT PROPERTY",  #TRANSLATED:: PROPERTI INVESTASI
																		4:"GOODWILL/NET", #ASET TETAP NILAI BUKU
																		5:"OTHER INTANGIBLES, NET",
																		6:"OTHER DEFERRED TAXES",
																		7:"OTHER NON-CURRENT ASSETS",
																		8:"TOTAL"}

			self.labels_excel_match_LIABILITIES_CURRENT = {
																		1:"ACCOUNTS PAYABLE", #FROM CURRENT ## HUTANG USAHA
																		2:"SHORT-TERM DEBT", #HUTANG BANK (BANK LOAN)
																		3:"CURRENT PORTION OF LONG-TERM DEBT",
																		4:"ACCRUED LIABILITIES", #FROM CURRENT ## BIAYA YANG MASIH HARUS DIBAYAR (ACCRUED COST)
																		5:"INCOME TAXES PAYABLE",  #FROM CURRENT ## HUTANG PAJAK (TAX PAYABLE)
																		6:"OTHER PAYABLES", #FROM CURRENT ## HUTANG LAIN-LAIN(OTHER DEBTS)
																		7:"TOTAL"}

			self.labels_excel_match_LIABILITIES_NONCURRENT = {
																		1:"BANK LOAN", # HUTANG BANK #UTANG BANK
																		2:"OTHER LONG TERM LOAN", # HUTANG PEMEGANG SAHAM(SHAREHOLDER'S DEBT) #UTANG LAIN LAIN(OTHER PAYABLE)
																		3:"TOTAL"}

			self.labels_excel_match_LIABILITIES = {
																		1:"DEFFERED INCOME TAXES PAYABLE", #TRANSLATED-- HUTANG PAJAK PENGHASILAN DITANGGUHKAN
																		2:'OTHER DEFERRED LIABILITIES', #TRANSLATED KEWAJIBAN DITANGGUHKAN LAIN LAIN
																		3:"OTHER LIABILITIES", # KEWAJIBAN LAIN LAIN(OTHER LIABILITIES)
																		4:"TOTAL"
																			}

			self.labels_excel_match_EQUITY = {
																		1:"PREFERRED STOCK", #MODAL SAHAM
																		2:"COMMON STOCK/PAR VALUE PLUS ADDITIONAL PAID-IN CAPITAL",
																		3:"RETAINED EARNINGS (ACCUMULATED DEFICIT)", #SALDO LABA
																		4:"OTHER STOCKHOLDERS' EQUITY",
																		5:"TOTAL"}


	def extract_key_from_value_self(self,subcateg,to_find):
		label = "self.labels_excel_match_"+subcateg
		#print(label)
		#print(eval(label))
		for i,value in eval(label).items():
			if value==to_find:
				return i
		return 99


	def train_naive_bayes(self,cat_type,new=False):
		'''
		cat_type can be: ASSETS_CURRENT, ASSETS_NONCURRENT, LIABILITIES_SHORT,LIABILITIES_CURRENT, LIABILITIES_LONG, EQUITY
		'''
		cat_type = cat_type.upper()
		if new == False:
			file_path = 'train_data/'+cat_type+'.train'
			if os.path.isfile(file_path):
				data = pd.read_csv(file_path,sep="@")
				print("READ the file for training: {}".format(file_path))
				y = data['label']
				x = data['text']

				###Start Preprocessing
				# characters level tf-idf inititator
				tfidf_vect_ngram_chars = TfidfVectorizer(analyzer='char_wb', token_pattern=r'\w{1,}', ngram_range=(2,3), max_features=5000)
				tfidf_vect_ngram_chars.fit(x) #train on all x for ngram

				#Save vectorizer.vocabulary
				vocabulary_filename = 'features_extracted/'+cat_type+"_feature.pkl"
				print("SAVE the file for ngram Vocabulary: {}".format(vocabulary_filename))
				pickle.dump(tfidf_vect_ngram_chars.vocabulary_,open(vocabulary_filename,"wb"))
				#del(tfidf_vect_ngram_chars)

				#Transform to ngram
				xtrain_tfidf_ngram_chars =  tfidf_vect_ngram_chars.transform(x)
				##tfidf_vect_ngram_chars.get_feature_names()  #to see ngrams

				#inititate the model which we want to train
				classifier = MultinomialNB()
				#classifier = GaussianNB()
				#classifier = BernoulliNB()
				#classifier = xgboost.XGBClassifier()

				# fit the training dataset on the classifier
				classifier.fit(xtrain_tfidf_ngram_chars, y)

		    #save the model to disk
				model_filename = 'models/'+cat_type+'_model.sav'
				pickle.dump(classifier, open(model_filename, 'wb'))
				print("SAVE the Trained Model: {}".format(model_filename))
				return True
			else:
				print("Entered wrong category, No file found with category = {}".format(cat_type))
				return False
		else:
			print("set new to blank for default")
			return False

	def predict_subcat(self,cat_type,raw_x):
		raw_x = [x.upper() for x in raw_x]
		cat_type = cat_type.upper()
		#valid_categ = ['ASSETS_CURRENT', 'ASSETS_NONCURRENT', 'LIABILITIES_CURRENT','LIABILITIES_NONCURRENT','LIABILITIES','EQUITY']
		if cat_type in self.valid_categ:
			##If block only for all correct category type::
			model_file_path = "models/"+cat_type+"_model.sav"
			if os.path.isfile(model_file_path):

				#globs = globals()
				#locs = locals()

				#load the model from disk
				classifier = pickle.load(open(model_file_path, 'rb'))

				##Load the vectorizer vocabulary
				vocabulary_filename = 'features_extracted/'+cat_type+"_feature.pkl"
				if os.path.isfile(vocabulary_filename):
					tfidf_vect_ngram_chars = CountVectorizer(analyzer='char_wb', ngram_range=(2,3), max_features=5000,vocabulary=pickle.load(open(vocabulary_filename, "rb")))

					#perform ngram on predict type
					xvalid_tfidf_ngram_chars =  tfidf_vect_ngram_chars.transform(raw_x)

					#predict the labels on validation dataset
					predictions = classifier.predict(xvalid_tfidf_ngram_chars) #predict the  subcategory from preprocessed input received
					#print("self.labels_excel_match_"+cat_type+"[j]")
					#print("found category",[(raw_x[i],eval("self.labels_excel_match_"+cat_type+"[j]",globals(),locals())) for i,j in enumerate(predictions)])

					to_send = list()
					label = "self.labels_excel_match_"+cat_type
					for i,j in enumerate(predictions):
						to_send.append(eval(label+"[j]")) #append the list to be return using eval
						#print("found sub-category for:",raw_x[i],"--> ",eval(label+"[j]")) #print the raw and processed values
					return to_send

					#return [eval("self.labels_excel_match_"+cat_type+"[j]",globs,locs) for i,j in enumerate(predictions)]
				else:
					print("Vocabulary not found with Name: {}".format(vocabulary_filename))
			else:
					print("Model not found with Name: {}".format(model_file_path))
		else:
			if cat_type in ["ASSETS","LIABILITIES_EQUITY","EQUITY"] and len(raw_x)==1 and raw_x[0]=="TOTAL":
				return ['TOTAL']
			elif cat_type == "ASSETS" and len(raw_x)==2:
				if raw_x[0]=="ASET LAIN LAIN" and raw_x[1]== 'TOTAL':
					return ['Total','Other Assets']
				elif raw_x[1]=="ASET LAIN LAIN" and raw_x[0]== 'TOTAL':
					return ["Other Assets",'Total']
		print("Category input:{} not in valid categories {}".format(cat_type,self.valid_categ))
		return 1

if __name__ == '__main__':

	globs = globals()
	locs = locals()


	x_test = ['KAS SETARA','PIUTANG LAIN']
	y_test = ['Cash and Cash Equivalent', 'Accounts Receivable']

	p = classify()

	p.train_naive_bayes("ASSETS_CURRENT")
	y_predict =p.predict_subcat("ASSETS_CURRENT",x_test)


	p.predict_subcat("ASSETS_CURRENT",x_test)

	dd_test = ["as","ddd"]
	fi = "dd"
	print(eval(fi+"_test"))

	predictions = [1,2,3,4]
	labels_excel_match_ASSETS_CURRENT = ['a','b','c','d']
	cat_type = "ASSETS_CURRENT"
	eval(["labels_excel_match_"+cat_type+"[j]" for i,j in enumerate(predictions)])

	[eval("labels_excel_match_"+cat_type+"[i]") for i,j in enumerate(predictions)]
